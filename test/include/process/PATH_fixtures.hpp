/// Preamble {{{
//  ==========================================================================
//        @file PATH_fixtures.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-27 Saturday 02:25:09 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-08-23 Tuesday 22:40:47 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_PATH_FIXTURES_HPP_INCLUDED
# define PROCESS_PATH_FIXTURES_HPP_INCLUDED
//
# include <string>
// clang-format on
//
namespace process {
class PATH_fixture {
protected:
  ~PATH_fixture();
  //
protected:
  PATH_fixture();
  //
protected:
  char const*
  get() const;
  //
protected:
  ::std::string const path;
}; // class PATH_fixture
//
struct not_set_PATH_fixture : public PATH_fixture {
  not_set_PATH_fixture();
}; // struct not_set_PATH_fixture
//
struct set_PATH_fixture : public PATH_fixture {
  set_PATH_fixture(char const* p = "Hello, World!");
}; // struct set_PATH_fixture
//
struct empty_PATH_fixture : public PATH_fixture {
  empty_PATH_fixture();
}; // struct empty_PATH_fixture
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_PATH_FIXTURES_HPP_INCLUDED
