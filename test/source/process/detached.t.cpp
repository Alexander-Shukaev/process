/// Preamble {{{
//  ==========================================================================
//        @file detached.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 12:36:42 (+0100)
//  --------------------------------------------------------------------------
//     @created 2017-02-13 Monday 02:08:07 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/detached>
//
# include <process/child>
# include <process/closed_stream>
# include <process/exe>
# include <process/stde>
# include <process/stdi>
# include <process/stdo>
# include <process/system_error>
//
# include <process/t>
//
# include <chrono>
# include <thread>
// clang-format on
//
using std::chrono::milliseconds;
using std::errc;
using std::make_error_code;
using std::this_thread::sleep_for;
using std::thread;
//
namespace process {
namespace {
struct predicates {
  static T_CHECK_EX_PREDICATE(closed_stream) {
    T_CHECK_EQ(e.what(), "Closed stream");
    return true;
  }
  //
  static T_CHECK_EX_PREDICATE(system_error) {
    T_CHECK_EQ(e.code(), ::make_error_code(::errc::invalid_argument));
    T_CHECK_EQ(e.what(), "Invalid argument");
    return true;
  }
};
//
T_TEST_SUITE(detached)
//
T_TEST_CASE(default) {
  child c("./loop.sh", detached(), stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.close());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  {
    ::thread t([&c] {
      ::sleep_for(::milliseconds(10));
      T_CHECK_NO_THROW(c.terminate());
    });
    while (c) {
    }
    t.join();
  }
  T_CHECK_F(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(default_and_detach) {
  child c("./loop.sh", detached(), stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_EX(c.detach(), system_error, predicates);
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  {
    ::thread t([&c] {
      ::sleep_for(::milliseconds(10));
      T_CHECK_NO_THROW(c.terminate());
    });
    while (c) {
    }
    t.join();
  }
  T_CHECK_F(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(default_and_open) {
  child c;
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.open(
      "./loop.sh", detached(), stde::pipe(), stdi::pipe(), stdo::pipe()));
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.close());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  {
    ::thread t([&c] {
      ::sleep_for(::milliseconds(10));
      T_CHECK_NO_THROW(c.terminate());
    });
    while (c) {
    }
    t.join();
  }
  T_CHECK_F(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(true) {
  child c(
      "./loop.sh", detached(true), stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.close());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  {
    ::thread t([&c] {
      ::sleep_for(::milliseconds(10));
      T_CHECK_NO_THROW(c.terminate());
    });
    while (c) {
    }
    t.join();
  }
  T_CHECK_F(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(true_and_detach) {
  child c(
      "./loop.sh", detached(true), stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_EX(c.detach(), system_error, predicates);
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  {
    ::thread t([&c] {
      ::sleep_for(::milliseconds(10));
      T_CHECK_NO_THROW(c.terminate());
    });
    while (c) {
    }
    t.join();
  }
  T_CHECK_F(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(true_and_open) {
  child c;
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.open(
      "./loop.sh", detached(true), stde::pipe(), stdi::pipe(), stdo::pipe()));
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.close());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  {
    ::thread t([&c] {
      ::sleep_for(::milliseconds(10));
      T_CHECK_NO_THROW(c.terminate());
    });
    while (c) {
    }
    t.join();
  }
  T_CHECK_F(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(false) {
  child c(
      "./loop.sh", detached(false), stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.close());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(false_and_detach) {
  child c(
      "./loop.sh", detached(false), stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.detach());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_NO_THROW(c.close());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  {
    ::thread t([&c] {
      ::sleep_for(::milliseconds(10));
      T_CHECK_NO_THROW(c.terminate());
    });
    while (c) {
    }
    t.join();
  }
  T_CHECK_F(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(false_and_open) {
  child c;
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.open("./loop.sh",
                          detached(false),
                          stde::pipe(),
                          stdi::pipe(),
                          stdo::pipe()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.close());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
