/// Preamble {{{
//  ==========================================================================
//        @file read_operand.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 22:21:08 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-13 Friday 13:08:00 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef READ_OPERAND_HPP_INCLUDED
# define READ_OPERAND_HPP_INCLUDED
//
# include "../bad_exit_status.hpp"
//
# include <cstdlib>
//
# define READ_OPERAND(NAME, DISPLAY_NAME) \
    int NAME; \
    do { \
      if((::std::cin >> ::std::ws).peek() == \
         ::std::char_traits<char>::eof()) \
        return EXIT_SUCCESS; \
      if (!(::std::cin >> NAME)) { \
        if (::std::cin.eof()) \
          return EXIT_SUCCESS; \
        ::std::cerr << "Failed to read " #DISPLAY_NAME \
                       " operand: Not an integer" \
                    << ::std::endl; \
        return ::process::bad_exit_status; \
      } \
    } while (false)
//
# endif // READ_OPERAND_HPP_INCLUDED
