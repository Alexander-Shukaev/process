/// Preamble {{{
//  ==========================================================================
//        @file main.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 22:12:49 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-01 Sunday 12:10:13 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "read_operand.hpp"
# include "read_operator.hpp"
//
# include "../bad_exit_status.hpp"
//
# include <csignal>
# include <iostream>
// clang-format on
//
using process::bad_exit_status;
//
using std::cerr;
using std::cout;
using std::endl;
using std::exit;
using std::signal;
//
void
sigfpe_handler(int sig) {
  (void)sig;
  cerr << "Division by zero" << endl;
  exit(::bad_exit_status);
}
//
int
main() {
  signal(SIGFPE, sigfpe_handler);
  for (;;) {
    READ_OPERAND(a, first);
    READ_OPERATOR(o);
    READ_OPERAND(b, second);
    int c;
    switch (o) {
    case '+':
      c = a + b;
      break;
    case '-':
      c = a - b;
      break;
    case '*':
      c = a * b;
      break;
    case '/':
      c = a / b;
      break;
    default:
      cerr << "Invalid operator: " << o << endl;
      return ::bad_exit_status;
    }
    cout << a << o << b << '=' << c << endl;
  }
}
