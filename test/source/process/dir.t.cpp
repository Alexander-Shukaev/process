/// Preamble {{{
//  ==========================================================================
//        @file dir.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 22:24:28 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-12 Thursday 10:54:39 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/dir>
//
# include "bad_exit_status.hpp"
//
# include <process/child>
# include <process/closed_stream>
# include <process/exe>
# include <process/stde>
# include <process/stdi>
# include <process/stdo>
//
# include <process/t>
//
# include <boost/filesystem/operations.hpp>
// clang-format on
//
using boost::filesystem::canonical;
//
using std::flush;
using std::getline;
using std::string;
//
namespace process {
namespace {
struct predicates {
  static T_CHECK_EX_PREDICATE(closed_stream) {
    T_CHECK_EQ(e.what(), "Closed stream");
    return true;
  }
};
//
T_TEST_SUITE(dir)
//
T_TEST_CASE(string_constructor_and_remain) {
  child c("./dir.sh", dir("./xxx/../."), stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, ::canonical(".").string());
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(string_constructor_and_change) {
  child c("alu/process.t.alu",
          dir("alu"),
          stde::pipe(),
          stdi::pipe(),
          stdo::pipe());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  c.stdi() << '$' << ::flush;
  ::string s;
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "Failed to read first operand: Not an integer");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stde().eof());
  T_CHECK_EQ(c.wait(), bad_exit_status);
  T_CHECK_EQ(c.exit_status(), bad_exit_status);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
