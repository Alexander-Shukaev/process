/// Preamble {{{
//  ==========================================================================
//        @file env_var.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 12:50:39 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-09-28 Wednesday 23:19:09 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/env_var>
//
# include <process/PATH_fixtures>
//
# include <process/t>
// clang-format on
//
namespace process {
namespace {
T_TEST_SUITE(env_var)
//
T_TEST_CASE(name) {
  T_CHECK_EQ(env_var("XXX").name(), "XXX");
}
//
T_TEST_CASE_BY_FIXTURE(not_set_PATH, not_set_PATH_fixture) {
  T_CHECK_F(get());
  {
    env_var v("PATH");
    T_CHECK_F(v);
    T_CHECK_F(v.get());
    T_CHECK_F(v.value());
  }
  T_CHECK_F(get());
}
//
T_TEST_CASE_BY_FIXTURE(set_PATH, set_PATH_fixture) {
  T_CHECK_EQ(get(), "Hello, World!");
  {
    env_var v("PATH");
    T_CHECK_T(v);
    T_CHECK_T(v.get());
    T_CHECK_T(v.value());
    T_CHECK_EQ(v.get(), "Hello, World!");
    T_CHECK_EQ(v.value(), "Hello, World!");
  }
  T_CHECK_EQ(get(), "Hello, World!");
  {
    env_var v("PATH");
    v.set("$");
    T_CHECK_T(v);
    T_CHECK_T(v.get());
    T_CHECK_T(v.value());
    T_CHECK_EQ(v.get(), "$");
    T_CHECK_EQ(v.value(), "$");
  }
  T_CHECK_EQ(get(), "$");
  {
    env_var v("PATH");
    v.set(0);
    T_CHECK_F(v);
    T_CHECK_F(v.get());
    T_CHECK_F(v.value());
  }
  T_CHECK_F(get());
  {
    env_var v("PATH");
    v.set("");
    T_CHECK_T(v);
    T_CHECK_T(v.get());
    T_CHECK_T(v.value());
    T_CHECK_EQ(v.get(), "");
    T_CHECK_EQ(v.value(), "");
  }
  T_CHECK_F(get()[0]);
  {
    env_var v("PATH");
    v.reset();
    T_CHECK_F(v);
    T_CHECK_F(v.get());
    T_CHECK_F(v.value());
  }
  T_CHECK_F(get());
}
//
T_TEST_CASE_BY_FIXTURE(empty_PATH, empty_PATH_fixture) {
  T_CHECK_F(get()[0]);
  {
    env_var v("PATH");
    T_CHECK_T(v);
    T_CHECK_T(v.get());
    T_CHECK_T(v.value());
    T_CHECK_EQ(v.get(), "");
    T_CHECK_EQ(v.value(), "");
  }
  T_CHECK_F(get()[0]);
  {
    env_var v("PATH");
    v.set("$");
    T_CHECK_T(v);
    T_CHECK_T(v.get());
    T_CHECK_T(v.value());
    T_CHECK_EQ(v.get(), "$");
    T_CHECK_EQ(v.value(), "$");
  }
  T_CHECK_EQ(get(), "$");
  {
    env_var v("PATH");
    v.unset();
    T_CHECK_F(v);
    T_CHECK_F(v.get());
    T_CHECK_F(v.value());
  }
  T_CHECK_F(get());
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
