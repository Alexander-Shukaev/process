/// Preamble {{{
//  ==========================================================================
//        @file PATH_fixtures.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 12:51:18 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-08-23 Tuesday 22:44:21 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/PATH_fixtures>
//
# include <process/t>
//
# include <cstdlib>
# include <cstring>
// clang-format on
//
using std::strcmp;
//
namespace process {
PATH_fixture::~PATH_fixture() {
  T_CHECK_F(::setenv("PATH", path.c_str(), 1));
}
//
PATH_fixture::PATH_fixture() : path(::getenv("PATH")) {
}
//
char const*
PATH_fixture::get() const {
  return ::getenv("PATH");
}
//
not_set_PATH_fixture::not_set_PATH_fixture() {
  T_CHECK_F(::unsetenv("PATH"));
  T_CHECK_F(get());
}
//
set_PATH_fixture::set_PATH_fixture(char const* p) {
  T_CHECK_T(p);
  if (p)
    T_CHECK_F(::setenv("PATH", p, 1));
  auto v = get();
  T_CHECK_T(v);
  if (v)
    T_CHECK_F(::strcmp(v, p));
}
//
empty_PATH_fixture::empty_PATH_fixture() {
  T_CHECK_F(::setenv("PATH", "", 1));
  auto v = get();
  T_CHECK_T(v);
  if (v)
    T_CHECK_F(v[0]);
}
} // namespace process
