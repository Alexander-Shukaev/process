/// Preamble {{{
//  ==========================================================================
//        @file main.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 22:21:41 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-01 Sunday 12:10:13 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "../bad_exit_status.hpp"
//
# include <cstdlib>
# include <iostream>
// clang-format on
//
using process::bad_exit_status;
//
using std::cerr;
using std::char_traits;
using std::cin;
using std::endl;
using std::ws;
//
int
main() {
  for (;;) {
    if ((cin >> ws).peek() == char_traits<char>::eof())
      return EXIT_SUCCESS;
    char c;
    if (!(cin >> c)) {
      if (cin.eof())
        return EXIT_SUCCESS;
      cerr << "Standard input failed" << endl;
      return ::bad_exit_status;
    }
  }
}
