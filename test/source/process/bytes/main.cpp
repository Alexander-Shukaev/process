/// Preamble {{{
//  ==========================================================================
//        @file main.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 22:21:29 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-12 Thursday 21:00:37 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "../bad_exit_status.hpp"
//
# include <cstddef>
# include <iostream>
# include <string>
// clang-format on
//
using process::bad_exit_status;
//
using std::cin;
using std::cout;
using std::flush;
using std::size_t;
using std::stoi;
//
int
main(int argc, char* argv[]) {
  if (argc < 2)
    return ::bad_exit_status;
  try {
    auto m = static_cast<size_t>(stoi(argv[1]));
    for (size_t i = 0; i < m; ++i) {
      char c;
      cin >> c;
      cout << c << flush;
    }
  } catch (...) {
    return ::bad_exit_status;
  }
}
