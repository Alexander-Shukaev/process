/// Preamble {{{
//  ==========================================================================
//        @file exe_pathname.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 11:46:52 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-13 Friday 15:29:53 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/exe_pathname>
//
# include <process/env_error>
//
# include <process/PATH_fixtures>
//
# include <process/t>
// clang-format on
//
using std::initializer_list;
using std::string;
using std::vector;
//
namespace process {
namespace {
T_TEST_SUITE(exe_pathname)
//
T_TEST_CASE_BY_FIXTURE(not_set_PATH, not_set_PATH_fixture) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(env_error) {
      T_CHECK_EQ(e.what(), "Environment variable not set: PATH");
      return true;
    }
  };
  //
  T_CHECK_EX(exe_pathname("process.t.alu"), env_error, predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", string()), env_error, predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", ""), env_error, predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", static_cast<char const*>(0)),
             env_error,
             predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", {}), env_error, predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", ::initializer_list<::string>{}),
             env_error,
             predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", ::initializer_list<::string>()),
             env_error,
             predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", ::vector<::string>{}),
             env_error,
             predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", ::vector<::string>()),
             env_error,
             predicates);
}
//
T_TEST_CASE_BY_FIXTURE(set_PATH, set_PATH_fixture) {
  T_CHECK_EQ(exe_pathname("process.t.alu"), "");
  T_CHECK_EQ(exe_pathname("process.t.alu", string()), "");
  T_CHECK_EQ(exe_pathname("process.t.alu", ""), "");
  T_CHECK_EQ(exe_pathname("process.t.alu", static_cast<char const*>(0)), "");
  T_CHECK_EQ(exe_pathname("process.t.alu", {}), "");
  T_CHECK_EQ(exe_pathname("process.t.alu", ::initializer_list<::string>{}),
             "");
  T_CHECK_EQ(exe_pathname("process.t.alu", ::initializer_list<::string>()),
             "");
  T_CHECK_EQ(exe_pathname("process.t.alu", ::vector<::string>{}), "");
  T_CHECK_EQ(exe_pathname("process.t.alu", ::vector<::string>()), "");
}
//
T_TEST_CASE_BY_FIXTURE(empty_PATH, empty_PATH_fixture) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(env_error) {
      T_CHECK_EQ(e.what(), "Environment variable empty: PATH");
      return true;
    }
  };
  //
  T_CHECK_EX(exe_pathname("process.t.alu"), env_error, predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", string()), env_error, predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", ""), env_error, predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", static_cast<char const*>(0)),
             env_error,
             predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", {}), env_error, predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", ::initializer_list<::string>{}),
             env_error,
             predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", ::initializer_list<::string>()),
             env_error,
             predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", ::vector<::string>{}),
             env_error,
             predicates);
  T_CHECK_EX(exe_pathname("process.t.alu", ::vector<::string>()),
             env_error,
             predicates);
}
//
T_TEST_CASE(string) {
  T_CHECK_EQ(exe_pathname("process.t.alu", "xxx:.:.."), "");
  T_CHECK_EQ(exe_pathname("process.t.alu", "xxx:.:..:alu"),
             "alu/process.t.alu");
}
//
T_TEST_CASE(initializer_list) {
  T_CHECK_EQ(exe_pathname("process.t.alu", {"xxx", ".", ".."}), "");
  T_CHECK_EQ(exe_pathname("process.t.alu", {"xxx", ".", "..", "alu"}),
             "alu/process.t.alu");
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
