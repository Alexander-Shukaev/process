/// Preamble {{{
//  ==========================================================================
//        @file child.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 22:02:58 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-11 Wednesday 16:10:36 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/child>
//
# include <process/args>
# include <process/closed_stream>
# include <process/exe>
# include <process/no_exit_status>
# include <process/stde>
# include <process/stdi>
# include <process/stdo>
# include <process/system_error>
//
# include <process/t>
//
# include <boost/filesystem/operations.hpp>
//
# include <chrono>
# include <csignal>
# include <thread>
// clang-format on
//
using boost::filesystem::absolute;
//
using std::chrono::milliseconds;
using std::errc;
using std::getline;
using std::make_error_code;
using std::signal;
using std::string;
using std::this_thread::sleep_for;
using std::thread;
//
namespace process {
namespace {
struct predicates {
  static T_CHECK_EX_PREDICATE(closed_stream) {
    T_CHECK_EQ(e.what(), "Closed stream");
    return true;
  }
  //
  static T_CHECK_EX_PREDICATE(no_exit_status) {
    T_CHECK_EQ(e.what(), "No exit status");
    return true;
  }
};
//
struct signal_fixture {
  signal_fixture() {
    ::signal(SIGCHLD, SIG_IGN);
  }
  //
  ~signal_fixture() {
    ::signal(SIGCHLD, SIG_DFL);
  }
};
//
T_TEST_SUITE(child)
//
T_TEST_CASE(const) {
  child const c("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(const_operator_bool) {
  child const c;
  T_CHECK_F(bool(c));
}
//
T_TEST_CASE(const_operator_logical_not) {
  child const c;
  T_CHECK_F(c);
}
//
T_TEST_CASE(detached) {
  child const c;
  T_CHECK_F(c.detached());
}
//
T_TEST_CASE(joinable) {
  child const c;
  T_CHECK_F(c.joinable());
}
//
T_TEST_CASE(terminate_on_close) {
  child const c;
  T_CHECK_F(c.terminate_on_close());
}
//
T_TEST_CASE(open_and_join) {
  child c;
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(
      c.open("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe()));
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_Z(c.join());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_NO_THROW(
      c.open("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe()));
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_Z(c.join());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(open_and_terminate) {
  child c;
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(
      c.open("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe()));
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_NO_THROW(c.open("./loop.sh"));
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(get_args) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c;
  T_CHECK_EX(c.get_args(), system_error, predicates);
}
//
T_TEST_CASE(get_exe) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c;
  T_CHECK_EX(c.get_exe(), system_error, predicates);
}
//
T_TEST_CASE(exit_status) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c;
  T_CHECK_EX(c.exit_status(), system_error, predicates);
}
//
T_TEST_CASE(close) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c;
  T_CHECK_EX(c.close(), system_error, predicates);
}
//
T_TEST_CASE(detach) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c;
  T_CHECK_EX(c.detach(), system_error, predicates);
}
//
T_TEST_CASE(join) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c;
  T_CHECK_EX(c.join(), system_error, predicates);
}
//
T_TEST_CASE(terminate) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c;
  T_CHECK_EX(c.terminate(), system_error, predicates);
}
//
T_TEST_CASE(wait) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c;
  T_CHECK_EX(c.wait(), system_error, predicates);
}
//
T_TEST_CASE(close_true) {
  child c("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.close(true));
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(close_false) {
  child c("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.close(false));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(terminate_and_terminate) {
  child c("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_NO_THROW(c.terminate());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(terminate_and_detach) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::invalid_argument));
      T_CHECK_EQ(e.what(), "Invalid argument");
      return true;
    }
  };
  //
  child c("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.terminate());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_EX(c.detach(), system_error, predicates);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(terminate_and_join) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::invalid_argument));
      T_CHECK_EQ(e.what(), "Invalid argument");
      return true;
    }
  };
  //
  child c("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.terminate());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_EX(c.join(), system_error, predicates);
  T_CHECK_EX(c.exit_status(), no_exit_status, ::process::predicates);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(terminate_and_wait) {
  child c("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.terminate());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_EX(c.wait(), no_exit_status, predicates);
  T_CHECK_EX(c.exit_status(), no_exit_status, predicates);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(detach_and_terminate) {
  child c("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.detach());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(join_and_terminate) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  {
    ::thread t([&c] {
      ::sleep_for(::milliseconds(10));
      T_CHECK_NO_THROW(c.terminate());
    });
    T_CHECK_EX(c.join(), system_error, predicates);
    T_CHECK_EX(c.exit_status(), no_exit_status, ::process::predicates);
    t.join();
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(wait_and_terminate) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  {
    ::thread t([&c] {
      ::sleep_for(::milliseconds(10));
      T_CHECK_NO_THROW(c.terminate());
    });
    T_CHECK_EX(c.wait(), system_error, predicates);
    T_CHECK_EX(c.exit_status(), no_exit_status, ::process::predicates);
    t.join();
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(detach_and_detach) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::invalid_argument));
      T_CHECK_EQ(e.what(), "Invalid argument");
      return true;
    }
  };
  //
  child c("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.detach());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_EX(c.detach(), system_error, predicates);
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(detach_and_join) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::invalid_argument));
      T_CHECK_EQ(e.what(), "Invalid argument");
      return true;
    }
  };
  //
  child c("./loop.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./loop.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./loop.sh");
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.detach());
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_EX(c.join(), system_error, predicates);
  T_CHECK_EX(c.exit_status(), no_exit_status, ::process::predicates);
  T_CHECK_T(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(detach_and_wait) {
  child c("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_NO_THROW(c.detach());
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_T(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(join_and_detach) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::invalid_argument));
      T_CHECK_EQ(e.what(), "Invalid argument");
      return true;
    }
  };
  //
  child c("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_Z(c.join());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_EX(c.detach(), system_error, predicates);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(join_and_join) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::invalid_argument));
      T_CHECK_EQ(e.what(), "Invalid argument");
      return true;
    }
  };
  //
  child c("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_Z(c.join());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_EX(c.join(), system_error, predicates);
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(join_and_wait) {
  child c("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_Z(c.join());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(wait_and_detach) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::invalid_argument));
      T_CHECK_EQ(e.what(), "Invalid argument");
      return true;
    }
  };
  //
  child c("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_EX(c.detach(), system_error, predicates);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(wait_and_join) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::invalid_argument));
      T_CHECK_EQ(e.what(), "Invalid argument");
      return true;
    }
  };
  //
  child c("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_EX(c.join(), system_error, predicates);
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(wait_and_wait) {
  child c("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE_BY_FIXTURE(SIG_IGN_and_join, signal_fixture) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, ::absolute("./args.sh").string());
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_EX(c.join(), system_error, predicates);
  T_CHECK_EX(c.exit_status(), no_exit_status, ::process::predicates);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE_BY_FIXTURE(SIG_IGN_and_wait, signal_fixture) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::no_child_process));
      T_CHECK_EQ(e.what(), "No child processes");
      return true;
    }
  };
  //
  child c("./args.sh", stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, ::absolute("./args.sh").string());
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_EX(c.wait(), system_error, predicates);
  T_CHECK_EX(c.exit_status(), no_exit_status, ::process::predicates);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(is_stde_open) {
  child const c;
  T_CHECK_F(c.is_stde_open());
}
//
T_TEST_CASE(is_stdi_open) {
  child const c;
  T_CHECK_F(c.is_stdi_open());
}
//
T_TEST_CASE(is_stdo_open) {
  child const c;
  T_CHECK_F(c.is_stdo_open());
}
//
T_TEST_CASE(close_stde) {
  child c;
  T_CHECK_NO_THROW(c.close_stde());
}
//
T_TEST_CASE(close_stdi) {
  child c;
  T_CHECK_NO_THROW(c.close_stdi());
}
//
T_TEST_CASE(close_stdo) {
  child c;
  T_CHECK_NO_THROW(c.close_stdo());
}
//
T_TEST_CASE(stde) {
  child c;
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stdi) {
  child c;
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
}
//
T_TEST_CASE(stdo) {
  child c;
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(const_stde) {
  child const c;
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(const_stdi) {
  child const c;
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
}
//
T_TEST_CASE(const_stdo) {
  child const c;
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
