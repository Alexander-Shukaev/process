/// Preamble {{{
//  ==========================================================================
//        @file exe.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 21:54:50 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-10 Tuesday 16:29:29 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/exe>
//
# include <process/args>
# include <process/child>
# include <process/closed_stream>
# include <process/stdo>
# include <process/system_error>
//
# include <process/t>
//
# include <boost/filesystem/operations.hpp>
// clang-format on
//
using boost::filesystem::absolute;
//
using std::errc;
using std::getline;
using std::make_error_code;
using std::string;
//
namespace process {
namespace {
struct predicates {
  static T_CHECK_EX_PREDICATE(closed_stream) {
    T_CHECK_EQ(e.what(), "Closed stream");
    return true;
  }
};
//
T_TEST_SUITE(exe)
//
T_TEST_CASE(none) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::permission_denied));
      T_CHECK_EQ(e.what(), "Permission denied");
      return true;
    }
  };
  //
  T_CHECK_EX(child(exe("")), system_error, predicates);
  T_CHECK_EX(child(exe(0)), system_error, predicates);
}
//
T_TEST_CASE(initializer_list_constructor) {
  child c("./args.sh", stdo::pipe());
  T_CHECK_EQ(c.get_args().container(), args::container_type({"./args.sh"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, ::absolute("./args.sh").string());
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(initializer_list_constructor_and_args) {
  child c("./args.sh", args({"xxx", "1", "2", "3 4"}), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(),
             args::container_type({"xxx", "1", "2", "3 4"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, ::absolute("./args.sh").string());
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "3 4");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(no_such_file_or_directory) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(),
                 ::make_error_code(::errc::no_such_file_or_directory));
      T_CHECK_EQ(e.what(), "No such file or directory");
      return true;
    }
  };
  //
  T_CHECK_EX(child("xxx"), system_error, predicates);
}
//
T_TEST_CASE(copy) {
  exe e1("Hello, World!");
  exe e2(e1);
  T_CHECK_EQ(e1.pathname(), e2.pathname());
  T_CHECK_EQ(e1.pathname(), "Hello, World!");
  T_CHECK_EQ(e2.pathname(), "Hello, World!");
}
//
T_TEST_CASE(empty) {
  // clang-format off
  T_CHECK_T(exe(""           ).pathname().empty());
  T_CHECK_T(exe(0            ).pathname().empty());
  T_CHECK_T(exe(::string()   ).pathname().empty());
  T_CHECK_F(exe("1"          ).pathname().empty());
  T_CHECK_F(exe(::string("1")).pathname().empty());
  // clang-format on
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
