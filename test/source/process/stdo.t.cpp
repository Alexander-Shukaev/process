/// Preamble {{{
//  ==========================================================================
//        @file stdo.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 21:54:51 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-12 Thursday 19:55:23 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/stdo>
//
# include <process/args>
# include <process/child>
# include <process/closed_stream>
# include <process/invalid_argument>
# include <process/stde>
# include <process/stdi>
# include <process/system_error>
//
# include <process/t>
//
# include <fstream>
// clang-format on
//
using std::getline;
using std::ifstream;
using std::io_errc;
using std::make_error_code;
using std::string;
//
namespace process {
namespace {
struct predicates {
  static T_CHECK_EX_PREDICATE(closed_stream) {
    T_CHECK_EQ(e.what(), "Closed stream");
    return true;
  }
  //
  static T_CHECK_EX_PREDICATE(invalid_argument) {
    T_CHECK_EQ(e.what(), "Invalid argument");
    return true;
  }
};
//
T_TEST_SUITE(stdo)
//
T_TEST_CASE(without) {
  child c({"./loop.sh"});
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(default_constructor) {
  child c({"./loop.sh"}, stdo());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(close) {
  child c({"./loop.sh"}, stdo::close());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(close_and_pipe) {
  child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"},
          stdo::close(),
          stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Hello, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(file) {
  child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"},
          stdo::file("stdo.log"));
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  ::ifstream ifs("stdo.log");
  ::string   s;
  ::getline(ifs, s);
  T_CHECK_EQ(s, "1: Hello, World!");
  ::getline(ifs, s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(ifs, s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ifs.eof());
}
//
T_TEST_CASE(file_as_pipe) {
  {
    child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"},
            stdo::file(""));
    T_CHECK_F(c.detached());
    T_CHECK_T(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_T(c.is_stdo_open());
    T_REQUIRE_NO_THROW(c.stdo());
    T_CHECK_T(c.stdo().good());
    ::string s;
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "1: Hello, World!");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "1: Goodbye, World!");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "");
    T_CHECK_T(c.stdo().eof());
    T_CHECK_Z(c.wait());
    T_CHECK_Z(c.exit_status());
    T_CHECK_F(c);
    T_CHECK_F(c.detached());
    T_CHECK_F(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_NO_THROW(c.stdo());
  }
  {
    child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"}, stdo::file(0));
    T_CHECK_F(c.detached());
    T_CHECK_T(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_T(c.is_stdo_open());
    T_REQUIRE_NO_THROW(c.stdo());
    T_CHECK_T(c.stdo().good());
    ::string s;
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "1: Hello, World!");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "1: Goodbye, World!");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "");
    T_CHECK_T(c.stdo().eof());
    T_CHECK_Z(c.wait());
    T_CHECK_Z(c.exit_status());
    T_CHECK_F(c);
    T_CHECK_F(c.detached());
    T_CHECK_F(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_NO_THROW(c.stdo());
  }
}
//
T_TEST_CASE(file_and_pipe) {
  child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"},
          stdo::file("stdo.log"),
          stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Hello, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stdo());
  ::ifstream ifs("stdo.log");
  ::getline(ifs, s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ifs.eof());
}
//
T_TEST_CASE(none) {
  child c({"./loop.sh"}, stdo::none());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(null) {
  child c({"alu/process.t.alu"}, stdi::close(), stdo::null());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(zero) {
  child c({"alu/process.t.alu"}, stdi::close(), stdo::zero());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(pipe) {
  child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"}, stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Hello, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(pipe_and_file) {
  child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"},
          stdo::pipe(),
          stdo::file("stdo.log"));
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stdo());
  ::ifstream ifs("stdo.log");
  ::getline(ifs, s);
  T_CHECK_EQ(s, "1: Hello, World!");
  ::getline(ifs, s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(ifs, s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ifs.eof());
}
//
T_TEST_CASE(pipe_and_pipe) {
  child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"},
          stdo::pipe(),
          stde::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_T(c.is_stde_open());
  T_REQUIRE_NO_THROW(c.stdo());
  T_REQUIRE_NO_THROW(c.stde());
  T_CHECK_T(c.stdo().good());
  T_CHECK_T(c.stde().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Hello, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "2: Hello, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "2: Goodbye, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stde().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_NO_THROW(c.stde());
}
//
T_TEST_CASE(pipe_and_close) {
  child c({"./loop.sh"}, stdo::pipe(), stdo::close());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
}
//
T_TEST_CASE(pipe_and_close_and_pipe) {
  child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"},
          stdo::pipe(),
          stdo::close(),
          stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Hello, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(pipe_and_stde_and_close) {
  child c(
      {"./loop.sh"}, stdo::pipe(), stde::pipe(), stdo::stde(stde::close()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(pipe_and_stderr_and_close) {
  child c(
      {"./loop.sh"}, stdo::pipe(), stde::pipe(), stdo::stderr(stde::close()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stde_and_default_constructor) {
  child c({"./loop.sh"}, stdo::stde(stde()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stde_and_close) {
  child c({"./loop.sh"}, stdo::stde(stde::close()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stde_and_none) {
  child c({"./loop.sh"}, stdo::stde(stde::none()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stde_and_null) {
  child c({"./loop.sh"}, stdo::stde(stde::null()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stde_and_zero) {
  child c({"./loop.sh"}, stdo::stde(stde::zero()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stde_and_pipe) {
  child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"},
          stdo::stde(stde::pipe()));
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stde());
  T_CHECK_T(c.stde().good());
  ::string s;
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "1: Hello, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "2: Hello, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "2: Goodbye, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stde().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stde());
}
//
T_TEST_CASE(stde_and_pipe_and_stdo_and_pipe) {
  child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"},
          stdo::stde(stde::pipe()),
          stde::stdo(stdo::pipe()));
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_T(c.is_stde_open());
  T_REQUIRE_NO_THROW(c.stdo());
  T_REQUIRE_NO_THROW(c.stde());
  T_CHECK_T(c.stdo().good());
  T_CHECK_T(c.stde().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Hello, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2: Hello, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "2: Hello, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2: Goodbye, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "2: Goodbye, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stde().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_NO_THROW(c.stde());
}
//
T_TEST_CASE(stdo_and_stde_and_pipe) {
  T_CHECK_EX(child({"./loop.sh"}, stde::stdo(stdo::stde(stde::pipe()))),
             invalid_argument,
             predicates);
}
//
T_TEST_CASE(stdo_and_stde_and_close) {
  T_CHECK_EX(child({"./loop.sh"}, stde::stdo(stdo::stde(stde::close()))),
             invalid_argument,
             predicates);
}
//
T_TEST_CASE(stderr_and_default_constructor) {
  child c({"./loop.sh"}, stdo::stderr(stde()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stderr_and_close) {
  child c({"./loop.sh"}, stdo::stderr(stde::close()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stderr_and_none) {
  child c({"./loop.sh"}, stdo::stderr(stde::none()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stderr_and_null) {
  child c({"./loop.sh"}, stdo::stderr(stde::null()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stderr_and_zero) {
  child c({"./loop.sh"}, stdo::stderr(stde::zero()));
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_EX(c.stde(), closed_stream, predicates);
}
//
T_TEST_CASE(stderr_and_pipe) {
  child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"},
          stdo::stderr(stde::pipe()));
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stde());
  T_CHECK_T(c.stde().good());
  ::string s;
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "1: Hello, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "2: Hello, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "2: Goodbye, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stde().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stde());
}
//
T_TEST_CASE(stderr_and_pipe_and_stdout_and_pipe) {
  child c({"./stdo.sh", "Hello, World!", "Goodbye, World!"},
          stdo::stderr(stde::pipe()),
          stde::stdout(stdo::pipe()));
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_T(c.is_stde_open());
  T_REQUIRE_NO_THROW(c.stdo());
  T_REQUIRE_NO_THROW(c.stde());
  T_CHECK_T(c.stdo().good());
  T_CHECK_T(c.stde().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Hello, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2: Hello, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "2: Hello, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "1: Goodbye, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2: Goodbye, World!");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "2: Goodbye, World!");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stde().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_NO_THROW(c.stdo());
  T_CHECK_NO_THROW(c.stde());
}
//
T_TEST_CASE(stdout_and_stderr_and_pipe) {
  T_CHECK_EX(child({"./loop.sh"}, stde::stdout(stdo::stderr(stde::pipe()))),
             invalid_argument,
             predicates);
}
//
T_TEST_CASE(stdout_and_stderr_and_close) {
  T_CHECK_EX(child({"./loop.sh"}, stde::stdout(stdo::stderr(stde::close()))),
             invalid_argument,
             predicates);
}
//
T_TEST_CASE(already_open) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::io_errc::stream));
      T_CHECK_EQ(e.what(), "already open: iostream error");
      return true;
    }
  };
  //
  T_CHECK_EX(
      child({"xxx"}, stdo::pipe(), stdo::pipe()), system_error, predicates);
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
