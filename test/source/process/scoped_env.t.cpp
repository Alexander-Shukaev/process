/// Preamble {{{
//  ==========================================================================
//        @file scoped_env.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 12:46:54 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-08-27 Saturday 01:47:22 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/scoped_env>
//
# include <process/PATH_fixtures>
//
# include <process/t>
// clang-format on
//
namespace process {
namespace {
T_TEST_SUITE(scoped_env)
//
T_TEST_CASE_BY_FIXTURE(not_set_PATH, not_set_PATH_fixture) {
  T_CHECK_F(get());
  {
    scoped_env e({{"PATH", "$"}});
    T_CHECK_EQ(get(), "$");
  }
  T_CHECK_F(get());
}
//
T_TEST_CASE_BY_FIXTURE(set_PATH, set_PATH_fixture) {
  T_CHECK_EQ(get(), "Hello, World!");
  {
    scoped_env e({{"PATH", 0}});
    T_CHECK_F(get());
  }
  T_CHECK_EQ(get(), "Hello, World!");
  {
    scoped_env e({{"PATH", "$"}});
    T_CHECK_EQ(get(), "$");
  }
  T_CHECK_EQ(get(), "Hello, World!");
  {
    scoped_env e({{"PATH", ""}});
    T_CHECK_F(get()[0]);
  }
  T_CHECK_EQ(get(), "Hello, World!");
}
//
T_TEST_CASE_BY_FIXTURE(empty_PATH, empty_PATH_fixture) {
  T_CHECK_F(get()[0]);
  {
    scoped_env e({{"PATH", 0}});
    T_CHECK_F(get());
  }
  T_CHECK_F(get()[0]);
  {
    scoped_env e({{"PATH", "$"}});
    T_CHECK_EQ(get(), "$");
  }
  T_CHECK_F(get()[0]);
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
