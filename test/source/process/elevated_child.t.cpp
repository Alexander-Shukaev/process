/// Preamble {{{
//  ==========================================================================
//        @file elevated_child.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2018-09-28 Friday 02:11:17 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-11 Wednesday 16:10:36 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2018,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/elevated_child>
//
# include "bad_exit_status.hpp"
//
# include <process/args>
# include <process/closed_stream>
# include <process/credentials>
# include <process/exe>
# include <process/stde>
# include <process/stdi>
# include <process/stdo>
//
# include <process/t>
// clang-format on
//
using std::endl;
using std::flush;
using std::getline;
using std::string;
//
namespace process {
namespace {
struct predicates {
  static T_CHECK_EX_PREDICATE(closed_stream) {
    T_CHECK_EQ(e.what(), "Closed stream");
    return true;
  }
};
//
T_TEST_SUITE(elevated_child)
//
T_TEST_CASE(default_constructor) {
  elevated_child ec(args({"alu/process.t.alu"}), stde::pipe(), stdo::pipe());
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(default_constructor_and_stdi_pipe) {
  elevated_child ec(
      args({"alu/process.t.alu"}), stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(default_constructor_and_stdi_close) {
  elevated_child ec(
      args({"alu/process.t.alu"}), stde::pipe(), stdi::close(), stdo::pipe());
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(credentials_constructor) {
  elevated_child ec(credentials::env(),
                    args({"alu/process.t.alu"}),
                    stde::pipe(),
                    stdo::pipe());
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(credentials_constructor_and_stdi_close) {
  elevated_child ec(credentials::env(),
                    args({"alu/process.t.alu"}),
                    stde::pipe(),
                    stdi::close(),
                    stdo::pipe());
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(credentials_constructor_and_stdi_pipe) {
  elevated_child ec(credentials::env(),
                    args({"alu/process.t.alu"}),
                    stde::pipe(),
                    stdi::pipe(),
                    stdo::pipe());
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(default_open) {
  elevated_child ec;
  T_CHECK_F(ec);
  T_CHECK_F(ec.terminate_on_close());
  T_CHECK_F(ec.is_stde_open());
  T_CHECK_F(ec.is_stdi_open());
  T_CHECK_F(ec.is_stdo_open());
  T_CHECK_EX(ec.stde(), closed_stream, predicates);
  T_CHECK_EX(ec.stdi(), closed_stream, predicates);
  T_CHECK_EX(ec.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(
      ec.open(args({"alu/process.t.alu"}), stde::pipe(), stdo::pipe()));
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(default_open_and_stdi_close) {
  elevated_child ec;
  T_CHECK_F(ec);
  T_CHECK_F(ec.terminate_on_close());
  T_CHECK_F(ec.is_stde_open());
  T_CHECK_F(ec.is_stdi_open());
  T_CHECK_F(ec.is_stdo_open());
  T_CHECK_EX(ec.stde(), closed_stream, predicates);
  T_CHECK_EX(ec.stdi(), closed_stream, predicates);
  T_CHECK_EX(ec.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(ec.open(args({"alu/process.t.alu"}),
                           stde::pipe(),
                           stdi::close(),
                           stdo::pipe()));
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(default_open_and_stdi_pipe) {
  elevated_child ec;
  T_CHECK_F(ec);
  T_CHECK_F(ec.terminate_on_close());
  T_CHECK_F(ec.is_stde_open());
  T_CHECK_F(ec.is_stdi_open());
  T_CHECK_F(ec.is_stdo_open());
  T_CHECK_EX(ec.stde(), closed_stream, predicates);
  T_CHECK_EX(ec.stdi(), closed_stream, predicates);
  T_CHECK_EX(ec.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(ec.open(
      args({"alu/process.t.alu"}), stde::pipe(), stdi::pipe(), stdo::pipe()));
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(credentials_open) {
  elevated_child ec;
  T_CHECK_F(ec);
  T_CHECK_F(ec.terminate_on_close());
  T_CHECK_F(ec.is_stde_open());
  T_CHECK_F(ec.is_stdi_open());
  T_CHECK_F(ec.is_stdo_open());
  T_CHECK_EX(ec.stde(), closed_stream, predicates);
  T_CHECK_EX(ec.stdi(), closed_stream, predicates);
  T_CHECK_EX(ec.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(ec.open(credentials::env(),
                           args({"alu/process.t.alu"}),
                           stde::pipe(),
                           stdo::pipe()));
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(credentials_open_and_stdi_close) {
  elevated_child ec;
  T_CHECK_F(ec);
  T_CHECK_F(ec.terminate_on_close());
  T_CHECK_F(ec.is_stde_open());
  T_CHECK_F(ec.is_stdi_open());
  T_CHECK_F(ec.is_stdo_open());
  T_CHECK_EX(ec.stde(), closed_stream, predicates);
  T_CHECK_EX(ec.stdi(), closed_stream, predicates);
  T_CHECK_EX(ec.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(ec.open(credentials::env(),
                           args({"alu/process.t.alu"}),
                           stde::pipe(),
                           stdi::close(),
                           stdo::pipe()));
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(credentials_open_and_stdi_pipe) {
  elevated_child ec;
  T_CHECK_F(ec);
  T_CHECK_F(ec.terminate_on_close());
  T_CHECK_F(ec.is_stde_open());
  T_CHECK_F(ec.is_stdi_open());
  T_CHECK_F(ec.is_stdo_open());
  T_CHECK_EX(ec.stde(), closed_stream, predicates);
  T_CHECK_EX(ec.stdi(), closed_stream, predicates);
  T_CHECK_EX(ec.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(ec.open(credentials::env(),
                           args({"alu/process.t.alu"}),
                           stde::pipe(),
                           stdi::pipe(),
                           stdo::pipe()));
  T_CHECK_EQ(ec.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(ec.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_REQUIRE_NO_THROW(ec.stde());
  T_REQUIRE_NO_THROW(ec.stdi());
  T_REQUIRE_NO_THROW(ec.stdo());
  T_CHECK_T(ec.stde().good());
  T_CHECK_T(ec.stdi().good());
  T_CHECK_T(ec.stdo().good());
  ec.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ec.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(ec.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(ec.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(ec.stdo().eof());
  T_CHECK_T(ec.stde().eof());
  T_CHECK_EQ(ec.wait(), bad_exit_status);
  T_CHECK_EQ(ec.exit_status(), bad_exit_status);
  T_CHECK_F(ec);
  T_CHECK_T(ec.terminate_on_close());
  T_CHECK_T(ec.is_stde_open());
  T_CHECK_T(ec.is_stdi_open());
  T_CHECK_T(ec.is_stdo_open());
  T_CHECK_NO_THROW(ec.stde());
  T_CHECK_NO_THROW(ec.stdi());
  T_CHECK_NO_THROW(ec.stdo());
}
//
T_TEST_CASE(child_open) {
  elevated_child ec;
  child&         c(ec);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(
      c.open(args({"alu/process.t.alu"}), stde::pipe(), stdo::pipe()));
  T_CHECK_EQ(c.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(c.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  c.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  c.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stde().eof());
  T_CHECK_EQ(c.wait(), bad_exit_status);
  T_CHECK_EQ(c.exit_status(), bad_exit_status);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(child_open_and_stdi_close) {
  elevated_child ec;
  child&         c(ec);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.open(args({"alu/process.t.alu"}),
                          stde::pipe(),
                          stdi::close(),
                          stdo::pipe()));
  T_CHECK_EQ(c.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(c.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  c.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  c.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stde().eof());
  T_CHECK_EQ(c.wait(), bad_exit_status);
  T_CHECK_EQ(c.exit_status(), bad_exit_status);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(child_open_and_stdi_pipe) {
  elevated_child ec;
  child&         c(ec);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_F(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_F(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_EX(c.stdo(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.open(
      args({"alu/process.t.alu"}), stde::pipe(), stdi::pipe(), stdo::pipe()));
  T_CHECK_EQ(c.get_args().container(),
             args::container_type({"/usr/bin/sudo",
                                   "-u",
                                   credentials::username_env_var.get_or(),
                                   "-p",
                                   "\n",
                                   "-k",
                                   "-S",
                                   "alu/process.t.alu"}));
  T_CHECK_EQ(c.get_exe().pathname(), "/usr/bin/sudo");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  c.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  c.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stde().eof());
  T_CHECK_EQ(c.wait(), bad_exit_status);
  T_CHECK_EQ(c.exit_status(), bad_exit_status);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
