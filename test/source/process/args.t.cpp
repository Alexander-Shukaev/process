/// Preamble {{{
//  ==========================================================================
//        @file args.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 21:54:50 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-10 Tuesday 20:59:42 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/args>
//
# include <process/child>
# include <process/closed_stream>
# include <process/exe>
# include <process/stdo>
# include <process/system_error>
//
# include <process/t>
//
# include <boost/filesystem/operations.hpp>
// clang-format on
//
using boost::filesystem::absolute;
//
using std::errc;
using std::getline;
using std::initializer_list;
using std::make_error_code;
using std::string;
using std::vector;
//
namespace process {
namespace {
struct predicates {
  static T_CHECK_EX_PREDICATE(closed_stream) {
    T_CHECK_EQ(e.what(), "Closed stream");
    return true;
  }
};
//
T_TEST_SUITE(args)
//
T_TEST_CASE(none) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::errc::permission_denied));
      T_CHECK_EQ(e.what(), "Permission denied");
      return true;
    }
  };
  //
  T_CHECK_EX(child(args({})), system_error, predicates);
  T_CHECK_EX(child(args({""})), system_error, predicates);
  T_CHECK_EX(
      child(args(::initializer_list<::string>{})), system_error, predicates);
  // -------------------------------------------------------------------------
  T_CHECK_EX(child(args((::initializer_list<::string>()))),
             system_error,
             predicates);
  T_CHECK_EX((child(args(::initializer_list<::string>()))),
             system_error,
             predicates);
  //
  // CAUTION:
  //
  // Two variants of additional parentheses positioning to work around the
  // "most vexing parse" problem.
  // -------------------------------------------------------------------------
  T_CHECK_EX(child(args(::vector<::string>{})), system_error, predicates);
  // -------------------------------------------------------------------------
  T_CHECK_EX(child(args((::vector<::string>()))), system_error, predicates);
  T_CHECK_EX((child(args(::vector<::string>()))), system_error, predicates);
  //
  // CAUTION:
  //
  // Two variants of additional parentheses positioning to work around the
  // "most vexing parse" problem.
  // -------------------------------------------------------------------------
}
//
T_TEST_CASE(initializer_list_constructor) {
  child c({"./args.sh", "1", "2", "3 4"}, stdo::pipe());
  T_CHECK_EQ(c.get_args().container(),
             args::container_type({"./args.sh", "1", "2", "3 4"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, ::absolute("./args.sh").string());
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "3 4");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(initializer_list_constructor_and_exe) {
  child c({"xxx", "1", "2", "3 4"}, exe("./args.sh"), stdo::pipe());
  T_CHECK_EQ(c.get_args().container(),
             args::container_type({"xxx", "1", "2", "3 4"}));
  T_CHECK_EQ(c.get_exe().pathname(), "./args.sh");
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, ::absolute("./args.sh").string());
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "1");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "3 4");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(no_such_file_or_directory) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(),
                 ::make_error_code(::errc::no_such_file_or_directory));
      T_CHECK_EQ(e.what(), "No such file or directory");
      return true;
    }
  };
  //
  T_CHECK_EX(child(args({"xxx"})), system_error, predicates);
}
//
T_TEST_CASE(splice) {
  {
    ::vector<::string> v;
    T_CHECK_EQ(args(v).splice(0, {"2", "3"}).container(),
               args::container_type({"2", "3"}));
    T_CHECK_EQ(args(v).splice(1, {"2", "3"}).container(),
               args::container_type({"2", "3"}));
    T_CHECK_EQ(args(v).splice(2, {"2", "3"}).container(),
               args::container_type({"2", "3"}));
  }
  {
    ::vector<::string> v{"1"};
    T_CHECK_EQ(args(v).splice(0, {"2", "3"}).container(),
               args::container_type({"2", "3", "1"}));
    T_CHECK_EQ(args(v).splice(1, {"2", "3"}).container(),
               args::container_type({"1", "2", "3"}));
    T_CHECK_EQ(args(v).splice(2, {"2", "3"}).container(),
               args::container_type({"1", "2", "3"}));
  }
  {
    ::vector<::string> v{"1", "2"};
    T_CHECK_EQ(args(v).splice(0, {"3", "4"}).container(),
               args::container_type({"3", "4", "1", "2"}));
    T_CHECK_EQ(args(v).splice(1, {"3", "4"}).container(),
               args::container_type({"1", "3", "4", "2"}));
    T_CHECK_EQ(args(v).splice(2, {"3", "4"}).container(),
               args::container_type({"1", "2", "3", "4"}));
    T_CHECK_EQ(args(v).splice(3, {"3", "4"}).container(),
               args::container_type({"1", "2", "3", "4"}));
  }
  T_CHECK_EQ(args({}).splice(0, {"2", "3"}).container(),
             args::container_type({"2", "3"}));
  T_CHECK_EQ(args({}).splice(1, {"2", "3"}).container(),
             args::container_type({"2", "3"}));
  T_CHECK_EQ(args({}).splice(2, {"2", "3"}).container(),
             args::container_type({"2", "3"}));
  T_CHECK_EQ(args({"1"}).splice(0, {"2", "3"}).container(),
             args::container_type({"2", "3", "1"}));
  T_CHECK_EQ(args({"1"}).splice(1, {"2", "3"}).container(),
             args::container_type({"1", "2", "3"}));
  T_CHECK_EQ(args({"1"}).splice(2, {"2", "3"}).container(),
             args::container_type({"1", "2", "3"}));
  T_CHECK_EQ(args({"1", "2"}).splice(0, {"3", "4"}).container(),
             args::container_type({"3", "4", "1", "2"}));
  T_CHECK_EQ(args({"1", "2"}).splice(1, {"3", "4"}).container(),
             args::container_type({"1", "3", "4", "2"}));
  T_CHECK_EQ(args({"1", "2"}).splice(2, {"3", "4"}).container(),
             args::container_type({"1", "2", "3", "4"}));
  T_CHECK_EQ(args({"1", "2"}).splice(3, {"3", "4"}).container(),
             args::container_type({"1", "2", "3", "4"}));
}
//
T_TEST_CASE(copy_and_splice) {
  args a1({"1", "2"});
  args a2(a1);
  T_CHECK_EQ(a1.container(), a2.container());
  T_CHECK_NE(a1.splice(0, {"3", "4"}).container(), a2.container());
  T_CHECK_EQ(a1.container(), args::container_type({"3", "4", "1", "2"}));
  T_CHECK_EQ(a2.container(), args::container_type({"1", "2"}));
}
//
T_TEST_CASE(empty) {
  // clang-format off
  T_CHECK_T(args({                  }).container().empty());
  T_CHECK_F(args({"1"               }).container().empty());
  T_CHECK_F(args({"1", "2"          }).container().empty());
  T_CHECK_T(args(::vector<::string>()).container().empty());
  // clang-format on
}
//
T_TEST_CASE(size) {
  // clang-format off
  T_CHECK_EQ(args({                  }).container().size(), 0);
  T_CHECK_EQ(args({"1"               }).container().size(), 1);
  T_CHECK_EQ(args({"1", "2"          }).container().size(), 2);
  T_CHECK_EQ(args(::vector<::string>()).container().size(), 0);
  // clang-format on
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
