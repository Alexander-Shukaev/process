/// Preamble {{{
//  ==========================================================================
//        @file stdi.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 22:26:14 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-12 Thursday 18:52:39 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/stdi>
//
# include "bad_exit_status.hpp"
//
# include <process/args>
# include <process/child>
# include <process/closed_stream>
# include <process/stde>
# include <process/stdo>
# include <process/system_error>
//
# include <process/t>
//
# include <csignal>
# include <fstream>
// clang-format on
//
using std::endl;
using std::flush;
using std::getline;
using std::io_errc;
using std::make_error_code;
using std::ofstream;
using std::signal;
using std::size_t;
using std::string;
//
namespace process {
namespace {
struct predicates {
  static T_CHECK_EX_PREDICATE(closed_stream) {
    T_CHECK_EQ(e.what(), "Closed stream");
    return true;
  }
};
//
struct signal_fixture {
  signal_fixture() {
    ::signal(SIGPIPE, SIG_IGN);
  }
  //
  ~signal_fixture() {
    ::signal(SIGPIPE, SIG_DFL);
  }
};
//
T_TEST_SUITE(stdi)
//
T_TEST_CASE(without) {
  child c({"./loop.sh"});
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
}
//
T_TEST_CASE(default_constructor) {
  child c({"./loop.sh"}, stdi());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
}
//
T_TEST_CASE(close) {
  child c({"./loop.sh"}, stdi::close());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
}
//
T_TEST_CASE(close_and_pipe) {
  child c({"alu/process.t.alu"},
          stde::pipe(),
          stdi::close(),
          stdi::pipe(),
          stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  c.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  c.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stde().eof());
  T_CHECK_EQ(c.wait(), bad_exit_status);
  T_CHECK_EQ(c.exit_status(), bad_exit_status);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(file) {
  {
    ::ofstream ofs("stdi.in");
    // TODO:
    T_REQUIRE(bool(ofs));
    ofs << "2+3" << ::endl;
    ofs << "2/0" << ::endl;
  }
  child c({"alu/process.t.alu"},
          stde::pipe(),
          stdi::file("stdi.in"),
          stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdo().good());
  T_CHECK_EQ(c.wait(), bad_exit_status);
  T_CHECK_EQ(c.exit_status(), bad_exit_status);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stdo());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stde().eof());
}
//
T_TEST_CASE(file_as_pipe) {
  {
    child c(
        {"alu/process.t.alu"}, stde::pipe(), stdi::file(""), stdo::pipe());
    T_CHECK_F(c.detached());
    T_CHECK_T(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_T(c.is_stde_open());
    T_CHECK_T(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_REQUIRE_NO_THROW(c.stde());
    T_REQUIRE_NO_THROW(c.stdi());
    T_REQUIRE_NO_THROW(c.stdo());
    T_CHECK_T(c.stde().good());
    T_CHECK_T(c.stdi().good());
    T_CHECK_T(c.stdo().good());
    c.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
    ::string s;
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "2+3=5");
    c.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
    ::getline(c.stde(), s);
    T_CHECK_EQ(s, "Division by zero");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "");
    ::getline(c.stde(), s);
    T_CHECK_EQ(s, "");
    T_CHECK_T(c.stdo().eof());
    T_CHECK_T(c.stde().eof());
    T_CHECK_EQ(c.wait(), bad_exit_status);
    T_CHECK_EQ(c.exit_status(), bad_exit_status);
    T_CHECK_F(c);
    T_CHECK_F(c.detached());
    T_CHECK_F(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_T(c.is_stde_open());
    T_CHECK_T(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_NO_THROW(c.stde());
    T_CHECK_NO_THROW(c.stdi());
    T_CHECK_NO_THROW(c.stdo());
  }
  {
    child c({"alu/process.t.alu"}, stde::pipe(), stdi::file(0), stdo::pipe());
    T_CHECK_F(c.detached());
    T_CHECK_T(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_T(c.is_stde_open());
    T_CHECK_T(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_REQUIRE_NO_THROW(c.stde());
    T_REQUIRE_NO_THROW(c.stdi());
    T_REQUIRE_NO_THROW(c.stdo());
    T_CHECK_T(c.stde().good());
    T_CHECK_T(c.stdi().good());
    T_CHECK_T(c.stdo().good());
    c.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
    ::string s;
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "2+3=5");
    c.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
    ::getline(c.stde(), s);
    T_CHECK_EQ(s, "Division by zero");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "");
    ::getline(c.stde(), s);
    T_CHECK_EQ(s, "");
    T_CHECK_T(c.stdo().eof());
    T_CHECK_T(c.stde().eof());
    T_CHECK_EQ(c.wait(), bad_exit_status);
    T_CHECK_EQ(c.exit_status(), bad_exit_status);
    T_CHECK_F(c);
    T_CHECK_F(c.detached());
    T_CHECK_F(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_T(c.is_stde_open());
    T_CHECK_T(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_NO_THROW(c.stde());
    T_CHECK_NO_THROW(c.stdi());
    T_CHECK_NO_THROW(c.stdo());
  }
}
//
T_TEST_CASE(file_and_pipe) {
  {
    ::ofstream ofs("stdi.in");
    // TODO:
    T_REQUIRE(bool(ofs));
    ofs << "3/0" << ::endl;
    ofs << "4+2" << ::endl;
  }
  child c({"alu/process.t.alu"},
          stde::pipe(),
          stdi::file("stdi.in"),
          stdi::pipe(),
          stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  c.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  c.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stde().eof());
  T_CHECK_EQ(c.wait(), bad_exit_status);
  T_CHECK_EQ(c.exit_status(), bad_exit_status);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(none) {
  child c({"./loop.sh"}, stdi::none());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
}
//
T_TEST_CASE(null) {
  child c({"alu/process.t.alu"}, stdi::null());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
}
//
T_TEST_CASE(zero) {
  child c({"bytes/process.t.bytes", "4"}, stdi::zero(), stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  for (::size_t i = 0; i < 4; ++i) {
    char z;
    c.stdo() >> z;
    T_CHECK_EQ(z, '\0');
  }
  c.stdo().ignore();
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(pipe) {
  child c({"alu/process.t.alu"}, stde::pipe(), stdi::pipe(), stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  c.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  c.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stde().eof());
  T_CHECK_EQ(c.wait(), bad_exit_status);
  T_CHECK_EQ(c.exit_status(), bad_exit_status);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE_BY_FIXTURE(pipe_and_file, signal_fixture) {
  {
    ::ofstream ofs("stdi.in");
    // TODO:
    T_REQUIRE(bool(ofs));
    ofs << "4+2" << ::endl;
    ofs << "3/0" << ::endl;
  }
  child c({"alu/process.t.alu"},
          stde::pipe(),
          stdi::pipe(),
          stdi::file("stdi.in"),
          stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  // TODO:
  T_CHECK_F((c.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl));
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "4+2=6");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stdi().bad());
  T_CHECK_T(c.stde().eof());
  T_CHECK_EQ(c.wait(), bad_exit_status);
  T_CHECK_EQ(c.exit_status(), bad_exit_status);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(pipe_and_close) {
  child c({"./loop.sh"}, stdi::pipe(), stdi::close());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.terminate());
  while (c) {
  }
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
}
//
T_TEST_CASE(pipe_and_close_and_pipe) {
  child c({"alu/process.t.alu"},
          stde::pipe(),
          stdi::pipe(),
          stdi::close(),
          stdi::pipe(),
          stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_REQUIRE_NO_THROW(c.stde());
  T_REQUIRE_NO_THROW(c.stdi());
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stde().good());
  T_CHECK_T(c.stdi().good());
  T_CHECK_T(c.stdo().good());
  c.stdi() << '2' << ::flush << '+' << ::flush << 3 << ::endl;
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "2+3=5");
  c.stdi() << 2 << ::flush << '/' << ::flush << '0' << ::endl;
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "Division by zero");
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  ::getline(c.stde(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_T(c.stde().eof());
  T_CHECK_EQ(c.wait(), bad_exit_status);
  T_CHECK_EQ(c.exit_status(), bad_exit_status);
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stde_open());
  T_CHECK_T(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_NO_THROW(c.stde());
  T_CHECK_NO_THROW(c.stdi());
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE(close_stdi) {
  child c({"cin/process.t.cin"}, stdi::pipe());
  T_CHECK_T(c);
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_T(c.is_stdi_open());
  T_REQUIRE_NO_THROW(c.stdi());
  T_CHECK_T(c.stdi().good());
  T_CHECK_NO_THROW(c.close_stdi());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
}
//
T_TEST_CASE(already_open) {
  struct predicates {
    static T_CHECK_EX_PREDICATE(system_error) {
      T_CHECK_EQ(e.code(), ::make_error_code(::io_errc::stream));
      T_CHECK_EQ(e.what(), "already open: iostream error");
      return true;
    }
  };
  //
  T_CHECK_EX(
      child({"xxx"}, stdi::pipe(), stdi::pipe()), system_error, predicates);
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
