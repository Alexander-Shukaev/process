/// Preamble {{{
//  ==========================================================================
//        @file bad_exit_status.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 22:10:23 (+0100)
//  --------------------------------------------------------------------------
//     @created 2017-03-05 Sunday 22:08:14 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_BAD_EXIT_STATUS_HPP_INCLUDED
# define PROCESS_BAD_EXIT_STATUS_HPP_INCLUDED
// clang-format on
//
namespace process {
constexpr int bad_exit_status(42);
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_BAD_EXIT_STATUS_HPP_INCLUDED
