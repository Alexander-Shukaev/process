/// Preamble {{{
//  ==========================================================================
//        @file credentials.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2018-09-28 Friday 02:10:56 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-12 Thursday 17:04:03 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2018,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/credentials>
//
# include <process/args>
# include <process/env_var>
# include <process/scoped_env>
//
# include <process/t>
// clang-format on
//
namespace process {
namespace {
T_TEST_SUITE(credentials)
//
T_TEST_CASE(empty) {
  {
    credentials c;
    T_CHECK_T(c);
    T_CHECK_F(c.empty());
    T_CHECK_T(c.password().empty());
    T_CHECK_F(c.username().empty());
  }
  {
    credentials c(password("%^&*"));
    T_CHECK_T(c);
    T_CHECK_F(c.empty());
    T_CHECK_F(c.password().empty());
    T_CHECK_F(c.username().empty());
  }
  {
    credentials c(username("user"));
    T_CHECK_T(c);
    T_CHECK_F(c.empty());
    T_CHECK_T(c.password().empty());
    T_CHECK_F(c.username().empty());
  }
  {
    credentials c(username(""));
    T_CHECK_F(c);
    T_CHECK_T(c.empty());
    T_CHECK_T(c.password().empty());
    T_CHECK_T(c.username().empty());
  }
  {
    credentials c("%^&*", "user");
    T_CHECK_T(c);
    T_CHECK_F(c.empty());
    T_CHECK_F(c.password().empty());
    T_CHECK_F(c.username().empty());
  }
  {
    credentials c("", "");
    T_CHECK_F(c);
    T_CHECK_T(c.empty());
    T_CHECK_T(c.password().empty());
    T_CHECK_T(c.username().empty());
  }
  {
    credentials c(42);
    T_CHECK_T(c);
    T_CHECK_F(c.empty());
    T_CHECK_T(c.password().empty());
    T_CHECK_F(c.username().empty());
  }
  {
    credentials c("%^&*", 42);
    T_CHECK_T(c);
    T_CHECK_F(c.empty());
    T_CHECK_F(c.password().empty());
    T_CHECK_F(c.username().empty());
  }
  {
    credentials c("", 42);
    T_CHECK_T(c);
    T_CHECK_F(c.empty());
    T_CHECK_T(c.password().empty());
    T_CHECK_F(c.username().empty());
  }
}
//
T_TEST_CASE(to_elevation_args) {
  T_CHECK_EQ(credentials().to_elevation_args().container(),
             args::container_type(
                 {"/usr/bin/sudo", "-u", "root", "-p", "\n", "-k", "-S"}));
  T_CHECK_EQ(credentials(username("user")).to_elevation_args().container(),
             args::container_type(
                 {"/usr/bin/sudo", "-u", "user", "-p", "\n", "-k", "-S"}));
  T_CHECK_EQ(credentials("#", "user").to_elevation_args().container(),
             args::container_type(
                 {"/usr/bin/sudo", "-u", "user", "-p", "\n", "-k", "-S"}));
  T_CHECK_EQ(credentials("#", 42).to_elevation_args().container(),
             args::container_type(
                 {"/usr/bin/sudo", "-u", "#42", "-p", "\n", "-k", "-S"}));
  {
    args a({"1", "2"});
    T_CHECK_EQ(credentials().to_elevation_args(a).container(),
               args::container_type({"/usr/bin/sudo",
                                     "-u",
                                     "root",
                                     "-p",
                                     "\n",
                                     "-k",
                                     "-S",
                                     "1",
                                     "2"}));
  }
  {
    args a({"1", "2"});
    T_CHECK_EQ(credentials(username("user")).to_elevation_args(a).container(),
               args::container_type({"/usr/bin/sudo",
                                     "-u",
                                     "user",
                                     "-p",
                                     "\n",
                                     "-k",
                                     "-S",
                                     "1",
                                     "2"}));
  }
  {
    args a({"1", "2"});
    T_CHECK_EQ(credentials("#", "user").to_elevation_args(a).container(),
               args::container_type({"/usr/bin/sudo",
                                     "-u",
                                     "user",
                                     "-p",
                                     "\n",
                                     "-k",
                                     "-S",
                                     "1",
                                     "2"}));
  }
  {
    args a({"1", "2"});
    T_CHECK_EQ(credentials("#", 42).to_elevation_args(a).container(),
               args::container_type({"/usr/bin/sudo",
                                     "-u",
                                     "#42",
                                     "-p",
                                     "\n",
                                     "-k",
                                     "-S",
                                     "1",
                                     "2"}));
  }
}
//
T_TEST_CASE(password) {
  {
    credentials c;
    T_CHECK_T(c.password().empty());
    T_CHECK_EQ(c.password(), "");
  }
  {
    credentials c(password("%^&*"));
    T_CHECK_F(c.password().empty());
    T_CHECK_EQ(c.password(), "%^&*");
  }
  {
    credentials c(username("user"));
    T_CHECK_T(c.password().empty());
    T_CHECK_EQ(c.password(), "");
  }
  {
    credentials c(username(""));
    T_CHECK_T(c.password().empty());
    T_CHECK_EQ(c.password(), "");
  }
  {
    credentials c("%^&*", "user");
    T_CHECK_F(c.password().empty());
    T_CHECK_EQ(c.password(), "%^&*");
  }
  {
    credentials c("", "");
    T_CHECK_T(c.password().empty());
    T_CHECK_EQ(c.password(), "");
  }
  {
    credentials c(42);
    T_CHECK_T(c.password().empty());
    T_CHECK_EQ(c.password(), "");
  }
  {
    credentials c("%^&*", 42);
    T_CHECK_F(c.password().empty());
    T_CHECK_EQ(c.password(), "%^&*");
  }
  {
    credentials c("", 42);
    T_CHECK_T(c.password().empty());
    T_CHECK_EQ(c.password(), "");
  }
  {
    scoped_env  e({{"PW", 0}});
    credentials c(password(env_var("PW")));
    T_CHECK_T(c.password().empty());
    T_CHECK_EQ(c.password(), "");
  }
  {
    scoped_env  e({{"PW", ""}});
    credentials c(password(env_var("PW")));
    T_CHECK_T(c.password().empty());
    T_CHECK_EQ(c.password(), "");
  }
  {
    scoped_env  e({{"PW", "%^&*"}});
    credentials c(password(env_var("PW")));
    T_CHECK_F(c.password().empty());
    T_CHECK_EQ(c.password(), "%^&*");
  }
}
//
T_TEST_CASE(username) {
  {
    credentials c;
    T_CHECK_F(c.username().empty());
    T_CHECK_EQ(c.username(), "root");
  }
  {
    credentials c(password("%^&*"));
    T_CHECK_F(c.username().empty());
    T_CHECK_EQ(c.username(), "root");
  }
  {
    credentials c(username("user"));
    T_CHECK_F(c.username().empty());
    T_CHECK_EQ(c.username(), "user");
  }
  {
    credentials c(username(""));
    T_CHECK_T(c.username().empty());
    T_CHECK_EQ(c.username(), "");
  }
  {
    credentials c("%^&*", "user");
    T_CHECK_F(c.username().empty());
    T_CHECK_EQ(c.username(), "user");
  }
  {
    credentials c("", "");
    T_CHECK_T(c.username().empty());
    T_CHECK_EQ(c.username(), "");
  }
  {
    credentials c(42);
    T_CHECK_F(c.username().empty());
    T_CHECK_EQ(c.username(), "#42");
  }
  {
    credentials c("%^&*", 42);
    T_CHECK_F(c.username().empty());
    T_CHECK_EQ(c.username(), "#42");
  }
  {
    credentials c("", 42);
    T_CHECK_F(c.username().empty());
    T_CHECK_EQ(c.username(), "#42");
  }
  {
    scoped_env  e({{"UN", 0}});
    credentials c(username(env_var("UN")));
    T_CHECK_T(c.username().empty());
    T_CHECK_EQ(c.username(), "");
  }
  {
    scoped_env  e({{"UN", ""}});
    credentials c(username(env_var("UN")));
    T_CHECK_T(c.username().empty());
    T_CHECK_EQ(c.username(), "");
  }
  {
    scoped_env  e({{"UN", "user"}});
    credentials c(username(env_var("UN")));
    T_CHECK_F(c.username().empty());
    T_CHECK_EQ(c.username(), "user");
  }
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
