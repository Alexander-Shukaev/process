/// Preamble {{{
//  ==========================================================================
//        @file env.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 21:54:50 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-11 Wednesday 07:21:32 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/env>
//
# include <process/args>
# include <process/child>
# include <process/closed_stream>
# include <process/stdo>
//
# include <process/t>
//
# include <cstdlib>
# include <cstring>
// clang-format on
//
using std::getline;
using std::initializer_list;
using std::strcmp;
using std::string;
using std::vector;
//
namespace process {
namespace {
struct predicates {
  static T_CHECK_EX_PREDICATE(closed_stream) {
    T_CHECK_EQ(e.what(), "Closed stream");
    return true;
  }
};
//
struct fixture {
  static ::string const var1_name;
  static ::string const var2_name;
  //
  static ::string const var1_value;
  static ::string const var2_value;
};
//
::string const fixture::var1_name("_PROCESS_ENV_VAR1");
::string const fixture::var2_name("_PROCESS_ENV_VAR2");
//
::string const fixture::var1_value("Hello, World!");
::string const fixture::var2_value("Goodbye, World!");
//
struct env_fixture : public fixture {
  ~env_fixture() {
    ::unsetenv(var1_name.c_str());
    ::unsetenv(var2_name.c_str());
    T_CHECK_F(::getenv(var1_name.c_str()));
    T_CHECK_F(::getenv(var2_name.c_str()));
  }
  //
  env_fixture() {
    ::setenv(var1_name.c_str(), var1_value.c_str(), 1);
    ::setenv(var2_name.c_str(), var2_value.c_str(), 1);
    T_CHECK_Z(::strcmp(::getenv(var1_name.c_str()), var1_value.c_str()));
    T_CHECK_Z(::strcmp(::getenv(var2_name.c_str()), var2_value.c_str()));
  }
};
//
T_TEST_SUITE(env)
//
T_TEST_CASE_BY_FIXTURE(none, env_fixture) {
  {
    child c({"./env.sh", var1_name, var2_name}, env({}), stdo::pipe());
    T_CHECK_F(c.detached());
    T_CHECK_T(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_REQUIRE_NO_THROW(c.stdo());
    T_CHECK_T(c.stdo().good());
    ::string s;
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var1_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var2_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "");
    T_CHECK_T(c.stdo().eof());
    T_CHECK_Z(c.wait());
    T_CHECK_Z(c.exit_status());
    T_CHECK_F(c);
    T_CHECK_F(c.detached());
    T_CHECK_F(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_CHECK_NO_THROW(c.stdo());
  }
  {
    child c({"./env.sh", var1_name, var2_name}, env({""}), stdo::pipe());
    T_CHECK_F(c.detached());
    T_CHECK_T(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_REQUIRE_NO_THROW(c.stdo());
    T_CHECK_T(c.stdo().good());
    ::string s;
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var1_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var2_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "");
    T_CHECK_T(c.stdo().eof());
    T_CHECK_Z(c.wait());
    T_CHECK_Z(c.exit_status());
    T_CHECK_F(c);
    T_CHECK_F(c.detached());
    T_CHECK_F(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_CHECK_NO_THROW(c.stdo());
  }
  {
    child c({"./env.sh", var1_name, var2_name}, env({"xxx"}), stdo::pipe());
    T_CHECK_F(c.detached());
    T_CHECK_T(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_REQUIRE_NO_THROW(c.stdo());
    T_CHECK_T(c.stdo().good());
    ::string s;
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var1_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var2_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "");
    T_CHECK_T(c.stdo().eof());
    T_CHECK_Z(c.wait());
    T_CHECK_Z(c.exit_status());
    T_CHECK_F(c);
    T_CHECK_F(c.detached());
    T_CHECK_F(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_CHECK_NO_THROW(c.stdo());
  }
  {
    child c({"./env.sh", var1_name, var2_name},
            env(::initializer_list<::string>{}),
            stdo::pipe());
    T_CHECK_F(c.detached());
    T_CHECK_T(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_REQUIRE_NO_THROW(c.stdo());
    T_CHECK_T(c.stdo().good());
    ::string s;
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var1_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var2_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "");
    T_CHECK_T(c.stdo().eof());
    T_CHECK_Z(c.wait());
    T_CHECK_Z(c.exit_status());
    T_CHECK_F(c);
    T_CHECK_F(c.detached());
    T_CHECK_F(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_CHECK_NO_THROW(c.stdo());
  }
  {
    child c({"./env.sh", var1_name, var2_name},
            env(::initializer_list<::string>()),
            stdo::pipe());
    T_CHECK_F(c.detached());
    T_CHECK_T(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_REQUIRE_NO_THROW(c.stdo());
    T_CHECK_T(c.stdo().good());
    ::string s;
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var1_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var2_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "");
    T_CHECK_T(c.stdo().eof());
    T_CHECK_Z(c.wait());
    T_CHECK_Z(c.exit_status());
    T_CHECK_F(c);
    T_CHECK_F(c.detached());
    T_CHECK_F(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_CHECK_NO_THROW(c.stdo());
  }
  {
    child c({"./env.sh", var1_name, var2_name},
            env(::vector<::string>{}),
            stdo::pipe());
    T_CHECK_F(c.detached());
    T_CHECK_T(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_REQUIRE_NO_THROW(c.stdo());
    T_CHECK_T(c.stdo().good());
    ::string s;
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var1_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var2_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "");
    T_CHECK_T(c.stdo().eof());
    T_CHECK_Z(c.wait());
    T_CHECK_Z(c.exit_status());
    T_CHECK_F(c);
    T_CHECK_F(c.detached());
    T_CHECK_F(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_CHECK_NO_THROW(c.stdo());
  }
  {
    child c({"./env.sh", var1_name, var2_name},
            env(::vector<::string>()),
            stdo::pipe());
    T_CHECK_F(c.detached());
    T_CHECK_T(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_REQUIRE_NO_THROW(c.stdo());
    T_CHECK_T(c.stdo().good());
    ::string s;
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var1_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, var2_name + "=");
    ::getline(c.stdo(), s);
    T_CHECK_EQ(s, "");
    T_CHECK_T(c.stdo().eof());
    T_CHECK_Z(c.wait());
    T_CHECK_Z(c.exit_status());
    T_CHECK_F(c);
    T_CHECK_F(c.detached());
    T_CHECK_F(c.joinable());
    T_CHECK_T(c.terminate_on_close());
    T_CHECK_F(c.is_stde_open());
    T_CHECK_F(c.is_stdi_open());
    T_CHECK_T(c.is_stdo_open());
    T_CHECK_EX(c.stde(), closed_stream, predicates);
    T_CHECK_EX(c.stdi(), closed_stream, predicates);
    T_CHECK_NO_THROW(c.stdo());
  }
}
//
T_TEST_CASE_BY_FIXTURE(default_constructor, env_fixture) {
  child c({"./env.sh", var1_name, var2_name}, env(), stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, var1_name + "=" + var1_value);
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, var2_name + "=" + var2_value);
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE_BY_FIXTURE(inherit, env_fixture) {
  child c({"./env.sh", var1_name, var2_name}, env::inherit(), stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, var1_name + "=" + var1_value);
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, var2_name + "=" + var2_value);
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_CASE_BY_FIXTURE(initializer_list_constructor, fixture) {
  child c({"./env.sh", var1_name, var2_name},
          env({var1_name + "=" + var1_value, var2_name + "=" + var2_value}),
          stdo::pipe());
  T_CHECK_F(c.detached());
  T_CHECK_T(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_REQUIRE_NO_THROW(c.stdo());
  T_CHECK_T(c.stdo().good());
  ::string s;
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, var1_name + "=" + var1_value);
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, var2_name + "=" + var2_value);
  ::getline(c.stdo(), s);
  T_CHECK_EQ(s, "");
  T_CHECK_T(c.stdo().eof());
  T_CHECK_Z(c.wait());
  T_CHECK_Z(c.exit_status());
  T_CHECK_F(c);
  T_CHECK_F(c.detached());
  T_CHECK_F(c.joinable());
  T_CHECK_T(c.terminate_on_close());
  T_CHECK_F(c.is_stde_open());
  T_CHECK_F(c.is_stdi_open());
  T_CHECK_T(c.is_stdo_open());
  T_CHECK_EX(c.stde(), closed_stream, predicates);
  T_CHECK_EX(c.stdi(), closed_stream, predicates);
  T_CHECK_NO_THROW(c.stdo());
}
//
T_TEST_SUITE_END
} // namespace
} // namespace process
