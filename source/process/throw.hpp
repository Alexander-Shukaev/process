/// Preamble {{{
//  ==========================================================================
//        @file throw.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-10 Tuesday 22:41:57 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-28 Thursday 22:58:43 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_THROW_HPP_INCLUDED
# define PROCESS_THROW_HPP_INCLUDED
//
# ifdef NDEBUG
#   define BOOST_EXCEPTION_DISABLE
# endif
# include <boost/throw_exception.hpp>
//
# include <boost/process/config.hpp>
# undef   BOOST_PROCESS_SOURCE_LOCATION
# define  BOOST_PROCESS_SOURCE_LOCATION
# undef   BOOST_PROCESS_THROW
# define  BOOST_PROCESS_THROW(EXCEPTION) BOOST_THROW_EXCEPTION(EXCEPTION)
//
# define PROCESS_LAST_SYSTEM_ERROR                                           \
    BOOST_PROCESS_LAST_ERROR
# define PROCESS_THROW(EXCEPTION)                                            \
    BOOST_PROCESS_THROW(EXCEPTION)
# define PROCESS_THROW_LAST_SYSTEM_ERROR(EXCEPTION)                          \
    BOOST_PROCESS_THROW_LAST_SYSTEM_ERROR(EXCEPTION)
//
# include "nested_exception.hpp"
//
# define PROCESS_THROW_WITH_NESTED(EXCEPTION)                                \
    PROCESS_THROW(::boost::enable_error_info(EXCEPTION)                      \
                  << PROCESS_NESTED_EXCEPTION_ERROR_INFO)
//
# endif // PROCESS_THROW_HPP_INCLUDED
