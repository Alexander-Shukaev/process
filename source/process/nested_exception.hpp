/// Preamble {{{
//  ==========================================================================
//        @file nested_exception.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-10 Tuesday 22:34:46 (+0100)
//  --------------------------------------------------------------------------
//     @created 2017-01-10 Tuesday 22:28:42 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_NESTED_EXCEPTION_HPP_INCLUDED
# define PROCESS_NESTED_EXCEPTION_HPP_INCLUDED
//
# include <boost/exception_ptr.hpp>
// clang-format on
//
namespace process {
typedef ::boost::error_info<struct nested_exception_tag,
                            ::boost::exception_ptr>
    nested_exception_error_info;
} // namespace process
//
namespace {}
//
// clang-format off
# define PROCESS_NESTED_EXCEPTION_ERROR_INFO                                 \
    ::process::nested_exception_error_info(::boost::current_exception())
//
# endif // PROCESS_NESTED_EXCEPTION_HPP_INCLUDED
