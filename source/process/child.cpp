/// Preamble {{{
//  ==========================================================================
//        @file child.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-05 Sunday 21:33:33 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 22:19:35 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "child.hpp"
//
# include "closed_stream.hpp"
# include "system_error.hpp"
# include "throw.hpp"
//
# include "implementation/child.hpp"
// clang-format on
//
using std::errc;
using std::istream;
using std::make_error_code;
using std::ostream;
//
namespace process {
namespace {
inline system_error
no_child_process_system_error() {
  return system_error(::make_error_code(::errc::no_child_process));
}
} // namespace
//
child::~child() = default;
//
child::child() = default;
//
child::child(args_type const& args, params_type const& params)
    : _i(new implementation::child(args, params),
         implementation::child::deleter()) {
}
//
child::child(exe_type const& exe, params_type const& params)
    : _i(new implementation::child(exe, params),
         implementation::child::deleter()) {
}
//
child::operator bool() const {
  return _i && _i.get<implementation::child>()->alive();
}
//
bool child::operator!() const {
  return !static_cast<bool>(*this);
}
//
void
child::open(args_type const& args, params_type const& params) {
  _i.reset(new implementation::child(args, params),
           implementation::child::deleter());
}
//
void
child::open(exe_type const& exe, params_type const& params) {
  _i.reset(new implementation::child(exe, params),
           implementation::child::deleter());
}
//
child::args_type const&
child::get_args() const {
  if (_i)
    return _i.get<implementation::child>()->_a;
  else
    PROCESS_THROW(no_child_process_system_error());
}
//
child::exe_type const&
child::get_exe() const {
  if (_i)
    return _i.get<implementation::child>()->_e;
  else
    PROCESS_THROW(no_child_process_system_error());
}
//
int
child::exit_status() const {
  if (_i)
    return _i.get<implementation::child>()->exit_status();
  else
    PROCESS_THROW(no_child_process_system_error());
}
//
bool
child::detached() const {
  return _i && _i.get<implementation::child>()->detached();
}
//
bool
child::joinable() const {
  return _i && _i.get<implementation::child>()->joinable();
}
//
bool
child::terminate_on_close() const {
  return _i && _i.get<implementation::child>()->_terminate_on_close;
}
//
void
child::close() {
  close(terminate_on_close());
}
//
void
child::close(bool terminate) {
  if (_i)
    _i.get<implementation::child>()->close(terminate);
  else
    PROCESS_THROW(no_child_process_system_error());
}
//
void
child::detach() {
  if (_i)
    _i.get<implementation::child>()->detach();
  else
    PROCESS_THROW(no_child_process_system_error());
}
//
int
child::join() {
  if (_i)
    return _i.get<implementation::child>()->join();
  else
    PROCESS_THROW(no_child_process_system_error());
}
//
void
child::terminate() {
  if (_i)
    _i.get<implementation::child>()->terminate();
  else
    PROCESS_THROW(no_child_process_system_error());
}
//
int
child::wait() const {
  if (_i)
    return _i.get<implementation::child>()->wait();
  else
    PROCESS_THROW(no_child_process_system_error());
}
//
bool
child::is_stde_open() const {
  return _i && _i.get<implementation::child>()->_stde.is_open();
}
//
bool
child::is_stdi_open() const {
  return _i && _i.get<implementation::child>()->_stdi.is_open();
}
//
bool
child::is_stdo_open() const {
  return _i && _i.get<implementation::child>()->_stdo.is_open();
}
//
void
child::close_stde() {
  if (is_stde_open())
    _i.get<implementation::child>()->_stde.close();
}
//
void
child::close_stdi() {
  if (is_stdi_open())
    _i.get<implementation::child>()->_stdi.close();
}
//
void
child::close_stdo() {
  if (is_stdo_open())
    _i.get<implementation::child>()->_stdo.close();
}
//
::istream const&
child::stde() const {
  if (is_stde_open())
    return _i.get<implementation::child>()->_stde;
  else
    PROCESS_THROW(closed_stream());
}
//
::istream&
child::stde() {
  if (is_stde_open())
    return _i.get<implementation::child>()->_stde;
  else
    PROCESS_THROW(closed_stream());
}
//
::ostream const&
child::stdi() const {
  if (is_stdi_open())
    return _i.get<implementation::child>()->_stdi;
  else
    PROCESS_THROW(closed_stream());
}
//
::ostream&
child::stdi() {
  if (is_stdi_open())
    return _i.get<implementation::child>()->_stdi;
  else
    PROCESS_THROW(closed_stream());
}
//
::istream const&
child::stdo() const {
  if (is_stdo_open())
    return _i.get<implementation::child>()->_stdo;
  else
    PROCESS_THROW(closed_stream());
}
//
::istream&
child::stdo() {
  if (is_stdo_open())
    return _i.get<implementation::child>()->_stdo;
  else
    PROCESS_THROW(closed_stream());
}
} // namespace process
