/// Preamble {{{
//  ==========================================================================
//        @file stdi.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-08 Wednesday 19:47:26 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-01 Sunday 01:29:21 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "stdi.hpp"
//
# include "null_device.hpp"
# include "param.hpp"
# include "zero_device.hpp"
//
# include "implementation/bind_stdi.hpp"
# include "implementation/close_stdi.hpp"
// clang-format on
//
using std::ostream;
using std::string;
//
namespace process {
stdi
stdi::close() {
  return stdi(new implementation::close_stdi);
}
//
stdi
stdi::none() {
  return stdi();
}
//
stdi
stdi::file(::string const& pathname, bool close_fds) {
  return stdi(new implementation::bind_stdi(pathname, close_fds));
}
//
stdi
stdi::file(char const* pathname, bool close_fds) {
  return stdi(
      new implementation::bind_stdi(pathname ? pathname : "", close_fds));
}
//
stdi
stdi::null() {
  return stdi(new implementation::bind_stdi(null_device(), true));
}
//
stdi
stdi::zero() {
  return stdi(new implementation::bind_stdi(zero_device(), true));
}
//
stdi
stdi::pipe(bool close_fds) {
  return stdi(new implementation::bind_stdi(close_fds));
}
//
stdi
stdi::pipe(::ostream& os, bool close_fds) {
  return stdi(new implementation::bind_stdi(os, close_fds));
}
//
stdi::stdi() = default;
//
stdi::stdi(void* p) : param(p) {
}
} // namespace process
