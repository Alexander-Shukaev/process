/// Preamble {{{
//  ==========================================================================
//        @file args.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-06 Tuesday 23:40:14 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-04-10 Sunday 11:11:10 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_ARGS_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_ARGS_HPP_INCLUDED
//
# include "param.hpp"
//
# include "platform.hpp"
# include PROCESS_IMPLEMENTATION_PLATFORM_MODULE(args)
          PROCESS_IMPLEMENTATION_PLATFORM_IMPORT(args)
//
# include <boost/optional.hpp>
// clang-format on
//
namespace process {
namespace implementation {
// ---------------------------------------------------------------------------
class args : public virtual param
//
// CAUTION:
//
// LLVM/Clang complains about `private' inheritance with the following error
// message:
//
//   error: inherited virtual base class 'process::implementation::param' has
//   private destructor
// ---------------------------------------------------------------------------
{
  typedef platform::args initializer_type;
  //
public:
  // clang-format off
  typedef ::std::initializer_list<::std::string> initializer_list_type;
  typedef ::std::            list<::std::string>        container_type;
  // clang-format on
  //
public:
  ~args();
  //
public:
  explicit args(initializer_list_type const& initializer_list);
  //
  explicit args(container_type const& container);
  //
public:
  args&
  splice(container_type::size_type index, container_type& container);
  //
  args&
  splice(container_type::size_type index, container_type&& container);
  //
public:
  container_type const&
  container() const;
  //
protected:
  virtual void
  initialize(interface_type const& interface, context_type& context) override;
  //
  virtual void
  adjust(interface_type const& interface, context_type& context) override;
  //
protected:
  virtual platform::initializer const&
  initializer() const override;
  //
private:
  container_type _c;
  //
private:
  ::boost::optional<initializer_type> mutable _i;
}; // class args
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_ARGS_HPP_INCLUDED
