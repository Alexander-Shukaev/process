/// Preamble {{{
//  ==========================================================================
//        @file dir.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-05-03 Tuesday 22:03:48 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-03 Tuesday 21:52:22 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_DIR_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_DIR_HPP_INCLUDED
//
# include "param.hpp"
//
# include "platform.hpp"
# include PROCESS_IMPLEMENTATION_PLATFORM_MODULE(dir)
          PROCESS_IMPLEMENTATION_PLATFORM_IMPORT(dir)
// clang-format on
//
namespace process {
namespace implementation {
class dir final : private param {
  typedef platform::dir initializer_type;
  //
public:
  explicit dir(::std::string const& pathname);
  //
private:
  virtual platform::initializer const&
  initializer() const override;
  //
private:
  initializer_type _i;
}; // class dir
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_DIR_HPP_INCLUDED
