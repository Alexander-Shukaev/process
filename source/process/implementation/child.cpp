/// Preamble {{{
//  ==========================================================================
//        @file child.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-02-12 Sunday 17:22:05 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-09 Tuesday 21:20:42 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "child.hpp"
//
# include "context.hpp"
# include "exited.hpp"
# include "get.hpp"
# include "initializer.hpp"
# include "initializers.hpp"
# include "not_exit_status.hpp"
# include "param.hpp"
# include "terminate.hpp"
# include "terminated.hpp"
# include "to_exit_status.hpp"
# include "wait.hpp"
//
# include "../rethrow.hpp"
//
# include <boost/process/execute.hpp>
//
# include <boost/scope_exit.hpp>
//
# include <functional>
# include <type_traits>
// clang-format on
//
using boost::process::execute;
//
using boost::process::initializers::close_fds;
using boost::process::initializers::inherit_env;
using boost::process::initializers::throw_on_error;
//
using std::errc;
using std::function;
using std::is_nothrow_destructible;
using std::make_error_code;
using std::size_t;
//
namespace process {
namespace {
inline system_error
invalid_argument_system_error() {
  return system_error(::make_error_code(::errc::invalid_argument));
}
//
class for_each_implementation {
  typedef implementation::child::params params;
  typedef ::function<void(
      ::size_t, param const&, implementation::param const&)>
      function;
  //
public:
  explicit for_each_implementation(param const&  param1,
                                   param const&  param2,
                                   params const& params);
  //
public:
  void
  operator()(function const& function) const;
  //
private:
  // clang-format off
  param  const& _param1;
  param  const& _param2;
  params const& _params;
  // clang-format on
}; // class for_each_implementation
//
inline for_each_implementation::for_each_implementation(param const&  param1,
                                                        param const&  param2,
                                                        params const& params)
    : _param1(param1), _param2(param2), _params(params) {
}
//
inline void
for_each_implementation::operator()(function const& function) const {
  ::size_t index(0);
  function(index++, _param1, implementation::get(_param1));
  function(index++, _param2, implementation::get(_param2));
  for (auto param : _params) {
    if (param)
      function(index++, *param, implementation::get(param));
  }
}
} // namespace
//
namespace implementation {
void
child::deleter::operator()(void* p) const {
  delete static_cast<child*>(p);
}
//
child::~child() {
  if (_terminate_on_close)
    try {
      terminate();
    } catch (...) {
    }
}
static_assert(::is_nothrow_destructible<child>::value, "Throw-destructible");
//
child::child(args const& a, params const& params)
    : basic_child(a)
    , _c(-1)
    , _terminate_on_close(true)
    , _terminated(false)
    , _waitable(_terminate_on_close.load())
    , _status(platform::not_exit_status) {
  execute(reinterpret_cast<param::interface_type const&>(_a),
          reinterpret_cast<param::interface_type const&>(_e),
          params);
}
//
child::child(exe const& e, params const& params)
    : basic_child(e)
    , _c(-1)
    , _terminate_on_close(true)
    , _terminated(false)
    , _waitable(_terminate_on_close.load())
    , _status(platform::not_exit_status) {
  execute(reinterpret_cast<param::interface_type const&>(_e),
          reinterpret_cast<param::interface_type const&>(_a),
          params);
}
//
int
child::exit_status() const {
  if (platform::exited(_status))
    return platform::to_exit_status(_status);
  PROCESS_THROW(no_exit_status());
}
//
bool
child::alive() const {
  if (terminated())
    return false;
  try {
    auto s      = platform::not_exit_status;
    _terminated = platform::terminated(_c, s);
    if (_terminated && platform::exited(s))
      _status = s;
    return !_terminated;
  }
  PROCESS_RETHROW_ALL
}
//
bool
child::detached() const noexcept {
  return valid() && !_terminate_on_close;
}
//
bool
child::joinable() const noexcept {
  return valid() && _waitable;
}
//
bool
child::valid() const noexcept {
  return _c.pid > 0;
}
//
bool
child::terminated() const noexcept {
  return !valid() || _terminated || platform::exited(_status);
}
//
int
child::wait() const {
  // TODO: As per [1, 2, 3], reevaluate and consider switching to a proper
  // C++11 scope guard solution (and most likely implement it in the C++ Hacks
  // project):
  BOOST_SCOPE_EXIT(this_) {
    this_->_waitable = false;
  }
  BOOST_SCOPE_EXIT_END
  if (!terminated()) {
    try {
      auto s = platform::not_exit_status;
      platform::wait(_c, s);
      _status = s;
    }
    PROCESS_RETHROW_ALL
  }
  return exit_status();
}
//
void
child::close(bool terminate_on_close) {
  // TODO: As per [1, 2, 3], reevaluate and consider switching to a proper
  // C++11 scope guard solution (and most likely implement it in the C++ Hacks
  // project):
  BOOST_SCOPE_EXIT(&terminate_on_close, this_) {
    if (terminate_on_close)
      this_->terminate();
  }
  BOOST_SCOPE_EXIT_END
  try {
    if (_stde.is_open())
      _stde.close();
    if (_stdi.is_open())
      _stdi.close();
    if (_stdo.is_open())
      _stdo.close();
  }
  PROCESS_RETHROW_ALL
}
//
void
child::detach() {
  if (!joinable())
    PROCESS_THROW(invalid_argument_system_error());
  // -------------------------------------------------------------------------
  //
  // NOTE:
  //
  // For compliance with the interface of `::std::thread'.
  // -------------------------------------------------------------------------
  _terminate_on_close = false;
  _waitable           = false;
}
//
int
child::join() {
  if (joinable())
    return wait();
  // -------------------------------------------------------------------------
  PROCESS_THROW(invalid_argument_system_error());
  //
  // NOTE:
  //
  // For compliance with the interface of `::std::thread'.
  // -------------------------------------------------------------------------
}
//
void
child::terminate() {
  if (!terminated()) {
    try {
      platform::terminate(_c);
    }
    PROCESS_RETHROW_ALL
  }
  _terminated = true;
  _waitable   = false;
}
//
void
child::execute(param::interface_type const& param1,
               param::interface_type const& param2,
               params const&                params) {
  try {
    context                 context(*this);
    for_each_implementation for_each_implementation(param1, param2, params);
    platform::initializers  initializers(2 + params.size());
    { // clang-format off
# define FOR_EACH_IMPLEMENTATION(F)                                          \
    for_each_implementation(                                                 \
        [&context](::size_t                     index,                       \
                   param::interface_type const& param_interface,             \
                   param const&                 param_implementation) {      \
          (void)index;                                                       \
          const_cast<param&>(param_implementation).F(param_interface,        \
                                                     context);               \
        })
    } // clang-format on
    FOR_EACH_IMPLEMENTATION(initialize);
    FOR_EACH_IMPLEMENTATION(adjust);
    for_each_implementation(
        [&initializers](::size_t                     index,
                        param::interface_type const& param_interface,
                        param const&                 param_implementation) {
          (void)index;
          (void)param_interface;
          if (param_implementation.has_initializer())
            initializers.push_back(const_cast<platform::initializer*>(
                &param_implementation.initializer()));
        });
    _c = ::execute(
        // -------------------------------------------------------------------
        ::inherit_env(),
        //
        // NOTE:
        //
        // For portability reasons, `inherit_env' is always used.  That is, on
        // POSIX, it is required to inherit environment variables from the
        // parent process, while, on Windows, this is the default behavior
        // regardless of `inherit_env' (i.e. it has empty implementation on
        // Windows).
        // -------------------------------------------------------------------
        reinterpret_cast<platform::initializer const&>(initializers),
        ::close_fds(context.close_fds.fds),
        ::throw_on_error());
    FOR_EACH_IMPLEMENTATION(finalize);
    { // clang-format off
# undef FOR_EACH_IMPLEMENTATION
    } // clang-format on
  }
  PROCESS_RETHROW_ALL
}
} // namespace implementation
} // namespace process
//
/// References {{{
//  ==========================================================================
//  [1] http://github.com/PeterSommerlad/SC22WG21_Papers
//  [2] http://github.com/nlguillemot/scope
//  [3] http://github.com/evgeny-panasyuk/stack_unwinding
//  ==========================================================================
//  }}} References
