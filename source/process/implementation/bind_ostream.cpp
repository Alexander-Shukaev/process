/// Preamble {{{
//  ==========================================================================
//        @file bind_ostream.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-08 Thursday 22:58:15 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-01 Sunday 01:39:58 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "bind_ostream.hpp"
//
# include "context.hpp"
//
# include "../closed_file_descriptor.hpp"
# include "../throw.hpp"
//
# include <boost/process/create_pipe.hpp>
// clang-format on
//
using boost::process::create_pipe;
//
using boost::iostreams::close_handle;
//
using boost::none;
using boost::optional;
//
using std::ostream;
//
namespace process {
namespace implementation {
bind_ostream::~bind_ostream() = default;
//
bind_ostream::bind_ostream() = default;
//
bind_ostream::bind_ostream(string_type const& pathname) : _p(pathname) {
}
//
bind_ostream::bind_ostream(::ostream& os) : _os(&os) {
}
//
::optional<::boost::process::pipe>
bind_ostream::initialize(interface_type const& interface,
                         context_type&         context,
                         ostream_type&         os) {
  (void)interface;
  (void)context;
  if (_p.empty()) {
    auto pipe = ::create_pipe();
    os.open(sink_type(pipe.sink, ::close_handle));
    _s.open(pipe.source, ::close_handle);
    if (_os)
      // TODO:
      os.rdbuf(_os->rdbuf());
    return pipe;
  } else {
    _s.open(_p);
  }
  return ::none;
}
//
void
bind_ostream::finalize(interface_type const& interface,
                       context_type&         context) {
  (void)interface;
  (void)context;
  if (_s.is_open())
    _s.close();
  else
    PROCESS_THROW(closed_file_descriptor());
}
} // namespace implementation
} // namespace process
