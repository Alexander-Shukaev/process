/// Preamble {{{
//  ==========================================================================
//        @file bind_stdo.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:44:30 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 23:35:57 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_BIND_STDO_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_BIND_STDO_HPP_INCLUDED
//
# include "bind_istream.hpp"
//
# include "platform.hpp"
# include PROCESS_IMPLEMENTATION_PLATFORM_MODULE(bind_stdo)
          PROCESS_IMPLEMENTATION_PLATFORM_IMPORT(bind_stdo)
//
#include <boost/optional.hpp>
// clang-format on
//
namespace process {
namespace implementation {
class bind_stdo : public virtual bind_istream {
  typedef platform::bind_stdo initializer_type;
  //
public:
  ~bind_stdo();
  //
public:
  explicit bind_stdo(bool close_fds);
  //
  explicit bind_stdo(string_type const& pathname, bool close_fds);
  //
  explicit bind_stdo(::std::istream& is, bool close_fds);
  //
public:
  virtual void*
  new_alternate() const override;
  //
  bool
  close_fds() const;
  //
protected:
  virtual void
  initialize(interface_type const& interface, context_type& context) override;
  //
  virtual void
  finalize(interface_type const& interface, context_type& context) override;
  //
protected:
  virtual platform::initializer const&
  initializer() const override;
  //
private:
  ::boost::optional<initializer_type> mutable _i;
  //
  bool const _close_fds;
}; // class bind_stdo
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_BIND_STDO_HPP_INCLUDED
