/// Preamble {{{
//  ==========================================================================
//        @file close_istream.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-06 Tuesday 00:00:30 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-06 Friday 21:50:04 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "close_istream.hpp"
// clang-format on
//
namespace process {
namespace implementation {
close_istream::close_istream() = default;
//
void
close_istream::initialize(interface_type const& interface,
                          context_type&         context,
                          istream_type&         is) {
  (void)interface;
  (void)context;
  if (is.is_open())
    is.close();
}
} // namespace implementation
} // namespace process
