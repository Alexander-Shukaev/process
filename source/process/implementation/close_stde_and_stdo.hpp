/// Preamble {{{
//  ==========================================================================
//        @file close_stde_and_stdo.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:55:35 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-06 Friday 19:52:18 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_CLOSE_STDE_AND_STDO_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_CLOSE_STDE_AND_STDO_HPP_INCLUDED
//
# include "close_stde.hpp"
# include "close_stdo.hpp"
//
# include "platform.hpp"
# include PROCESS_IMPLEMENTATION_PLATFORM_MODULE(pair_initializer)
          PROCESS_IMPLEMENTATION_PLATFORM_IMPORT(pair_initializer)
// clang-format on
//
namespace process {
namespace implementation {
class close_stde_and_stdo final : private close_stde, private close_stdo {
  typedef platform::pair_initializer initializer_type;
  //
public:
  explicit close_stde_and_stdo();
  //
private:
  virtual void*
  new_alternate() const override;
  //
private:
  virtual void
  initialize(interface_type const& interface, context_type& context) override;
  //
private:
  virtual platform::initializer const&
  initializer() const override;
  //
private:
  initializer_type _i;
}; // class close_stde_and_stdo
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_CLOSE_STDE_AND_STDO_HPP_INCLUDED
