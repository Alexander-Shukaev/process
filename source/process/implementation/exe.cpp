/// Preamble {{{
//  ==========================================================================
//        @file exe.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-10 Tuesday 01:09:27 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-05-03 Tuesday 22:55:47 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "exe.hpp"
//
# include "child.hpp"
# include "context.hpp"
//
# include "../param.hpp"
//
# include <boost/filesystem/operations.hpp>
// clang-format on
//
using boost::filesystem::absolute;
//
namespace process {
namespace implementation {
exe::~exe() = default;
//
exe::exe(pathname_type const& pathname) : _p(pathname) {
}
//
exe::pathname_type const&
exe::pathname() const {
  return _p;
}
//
void
exe::initialize(interface_type const& interface, context_type& context) {
  reinterpret_cast<interface_type&>(context.c._e).*PROCESS_PARAM_I_PTR =
      interface.*PROCESS_PARAM_I_PTR;
}
//
void
exe::adjust(interface_type const& interface, context_type& context) {
  // TODO: This should be moved to another place when point #1 is implemented.
  if (_i ||
      reinterpret_cast<interface_type&>(context.c._e).*PROCESS_PARAM_I_PTR !=
          interface.*PROCESS_PARAM_I_PTR)
    return;
  _i.emplace(
      // -------------------------------------------------------------------
      ::absolute(_p).string()
      //
      // NOTE:
      //
      // Use an absolute path with `exe' to avoid portability problems of
      // setting a child working directory (CWD) with `dir'.  That is, on
      // Windows, a relative path is relative to the current working directory
      // of the parent process, while, on POSIX, a relative path is relative
      // to the child working directory set with `dir' as the directory is
      // changed before the (child) program starts.
      // -------------------------------------------------------------------
      );
}
//
platform::initializer const&
exe::initializer() const {
  // TODO: This should unconditionally return `param::initializer()' when
  // point #1 is implemented.
  if (_i)
    return *_i;
  return param::initializer();
}
} // namespace implementation
} // namespace process
