/// Preamble {{{
//  ==========================================================================
//        @file bind_stdo_to_stde.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:54:14 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-04-28 Thursday 22:26:33 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "bind_stdo_to_stde.hpp"
//
# include "../throw.hpp"
# include "../unsupported_operation.hpp"
// clang-format on
//
namespace process {
namespace implementation {
bind_stdo_to_stde::bind_stdo_to_stde(bool close_fds)
    : bind_stdo(close_fds), bind_stde(close_fds) {
}
//
bind_stdo_to_stde::bind_stdo_to_stde(bind_stde const& e)
    : bind_istream(e.pathname())
    , bind_stdo(e.pathname(), e.close_fds())
    , bind_stde(e.pathname(), e.close_fds()) {
}
//
void*
bind_stdo_to_stde::new_alternate() const {
  PROCESS_THROW(unsupported_operation());
}
//
void
bind_stdo_to_stde::initialize(interface_type const& interface,
                              context_type&         context) {
  bind_stde::initialize(interface, context);
}
//
void
bind_stdo_to_stde::finalize(interface_type const& interface,
                            context_type&         context) {
  bind_stde::finalize(interface, context);
}
//
platform::initializer const&
bind_stdo_to_stde::initializer() const {
  if (!_i)
    _i.emplace(bind_stde::initializer(), bind_stdo::initializer());
  return *_i;
}
} // namespace implementation
} // namespace process
