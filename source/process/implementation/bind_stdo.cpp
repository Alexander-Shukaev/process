/// Preamble {{{
//  ==========================================================================
//        @file bind_stdo.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:46:09 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-09 Tuesday 21:20:42 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "bind_stdo.hpp"
//
# include "bind_stde_to_stdo.hpp"
# include "child.hpp"
# include "context.hpp"
// clang-format on
//
using std::istream;
//
namespace process {
namespace implementation {
bind_stdo::~bind_stdo() = default;
//
bind_stdo::bind_stdo(bool close_fds) : _close_fds(close_fds) {
}
//
bind_stdo::bind_stdo(string_type const& pathname, bool close_fds)
    : bind_istream(pathname), _close_fds(close_fds) {
}
//
bind_stdo::bind_stdo(::istream& is, bool close_fds)
    : bind_istream(is), _close_fds(close_fds) {
}
//
void*
bind_stdo::new_alternate() const {
  return new bind_stde_to_stdo(*this);
}
//
bool
bind_stdo::close_fds() const {
  return _close_fds;
}
//
void
bind_stdo::initialize(interface_type const& interface,
                      context_type&         context) {
  if ((context.bind_stdo.pipe =
           bind_istream::initialize(interface, context, context.c._stdo))) {
    if (_close_fds) {
      context.close(context.bind_stdo.pipe->source);
      context.close(context.bind_stdo.pipe->sink);
    }
  }
}
//
void
bind_stdo::finalize(interface_type const& interface, context_type& context) {
  bind_istream::finalize(interface, context);
}
//
platform::initializer const&
bind_stdo::initializer() const {
  return bind_istream::initializer(_i);
}
} // namespace implementation
} // namespace process
