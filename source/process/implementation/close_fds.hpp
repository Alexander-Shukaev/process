/// Preamble {{{
//  ==========================================================================
//        @file close_fds.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:42:42 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-22 Sunday 01:47:21 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_CLOSE_FDS_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_CLOSE_FDS_HPP_INCLUDED
//
# include "fd.hpp"
# include "param.hpp"
//
# include "platform.hpp"
# include PROCESS_IMPLEMENTATION_PLATFORM_MODULE(close_fds)
          PROCESS_IMPLEMENTATION_PLATFORM_IMPORT(close_fds)
//
#include <boost/optional.hpp>
// clang-format on
//
namespace process {
namespace implementation {
class close_fds : private virtual param {
  typedef platform::close_fds initializer_type;
  //
public:
  explicit close_fds();
  //
  explicit close_fds(::std::initializer_list<platform::fd> const& fds);
  //
  explicit close_fds(::std::vector<platform::fd> const& fds);
  //
protected:
  virtual void
  initialize(interface_type const& interface, context_type& context) override;
  //
  virtual void
  adjust(interface_type const& interface, context_type& context) override;
  //
protected:
  virtual platform::initializer const&
  initializer() const override;
  //
private:
  ::std::vector<platform::fd> _fds;
  //
private:
  ::boost::optional<initializer_type> mutable _i;
}; // class close_fds
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_CLOSE_FDS_HPP_INCLUDED
