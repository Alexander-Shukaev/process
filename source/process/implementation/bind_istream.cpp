/// Preamble {{{
//  ==========================================================================
//        @file bind_istream.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-08 Thursday 22:58:05 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-04-26 Tuesday 21:42:30 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "bind_istream.hpp"
//
# include "context.hpp"
//
# include "../closed_file_descriptor.hpp"
# include "../throw.hpp"
//
# include <boost/process/create_pipe.hpp>
// clang-format on
//
using boost::process::create_pipe;
//
using boost::iostreams::close_handle;
//
using boost::none;
using boost::optional;
//
using std::istream;
//
namespace process {
namespace implementation {
bind_istream::~bind_istream() = default;
//
bind_istream::bind_istream() = default;
//
bind_istream::bind_istream(string_type const& pathname) : _p(pathname) {
}
//
bind_istream::bind_istream(::istream& is) : _is(&is) {
}
//
::optional<::boost::process::pipe>
bind_istream::initialize(interface_type const& interface,
                         context_type&         context,
                         istream_type&         is) {
  (void)interface;
  (void)context;
  if (_p.empty()) {
    auto pipe = ::create_pipe();
    is.open(source_type(pipe.source, ::close_handle));
    _s.open(pipe.sink, ::close_handle);
    if (_is)
      // TODO:
      is.rdbuf(_is->rdbuf());
    return pipe;
  } else {
    _s.open(_p);
  }
  return ::none;
}
//
void
bind_istream::finalize(interface_type const& interface,
                       context_type&         context) {
  (void)interface;
  (void)context;
  if (_s.is_open())
    _s.close();
  else
    PROCESS_THROW(closed_file_descriptor());
}
} // namespace implementation
} // namespace process
