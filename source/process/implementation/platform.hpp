/// Preamble {{{
//  ==========================================================================
//        @file platform.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-04-10 Sunday 15:52:45 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 23:15:08 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_PLATFORM_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_PLATFORM_HPP_INCLUDED
//
# include <boost/system/api_config.hpp>
# if   defined(BOOST_POSIX_API)
#   define PROCESS_IMPLEMENTATION_PLATFORM posix
# elif defined(BOOST_WINDOWS_API)
#   define PROCESS_IMPLEMENTATION_PLATFORM windows
# else
#   error "Unknown platform"
# endif
# include <boost/preprocessor/stringize.hpp>
# define PROCESS_IMPLEMENTATION_PLATFORM_MODULE(PATHNAME) \
    BOOST_PP_STRINGIZE(PROCESS_IMPLEMENTATION_PLATFORM/PATHNAME.hpp)
# define PROCESS_IMPLEMENTATION_PLATFORM_IDENTIFIER(NAME) \
    ::process::implementation::PROCESS_IMPLEMENTATION_PLATFORM::NAME
# define PROCESS_IMPLEMENTATION_PLATFORM_IMPORT(NAME) \
    namespace process { \
    namespace implementation { \
    namespace platform { \
    using PROCESS_IMPLEMENTATION_PLATFORM_IDENTIFIER(NAME); \
    } \
    } \
    }
# define PROCESS_IMPLEMENTATION_PLATFORM_FORWARD_DECLARATION(NAME) \
    namespace process { \
    namespace implementation { \
    namespace PROCESS_IMPLEMENTATION_PLATFORM { \
    class NAME; \
    } \
    } \
    }
# define PROCESS_IMPLEMENTATION_PLATFORM_FORWARD_IMPORT(NAME) \
    PROCESS_IMPLEMENTATION_PLATFORM_FORWARD_DECLARATION(NAME) \
    PROCESS_IMPLEMENTATION_PLATFORM_IMPORT(NAME)
//
# endif // PROCESS_IMPLEMENTATION_PLATFORM_HPP_INCLUDED
