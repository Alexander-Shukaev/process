/// Preamble {{{
//  ==========================================================================
//        @file bind_ostream.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:49:06 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-01 Sunday 01:35:45 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_BIND_OSTREAM_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_BIND_OSTREAM_HPP_INCLUDED
//
# include "param.hpp"
//
# include "../closed_file_descriptor.hpp"
# include "../throw.hpp"
//
# include <process/firewall/deep_const_ptr>
//
# include <boost/process/pipe.hpp>
//
# include <boost/iostreams/device/file_descriptor.hpp>
# include <boost/iostreams/stream.hpp>
//
# include <boost/optional.hpp>
// clang-format on
//
namespace process {
namespace implementation {
// ---------------------------------------------------------------------------
class bind_ostream : public virtual param
//
// BUG:
//
// Removing `virtual' from inheritance above erroneously causes
// `param::initialize(context&)' to dispatch to `bind_stdo::initializer()'
// which is a complete nonsense.  Is it an undefined behavior resulting from
// violation of the C++ Standard or a GCC bug?
//
// CAUTION:
//
// LLVM/Clang complains about `private' inheritance with the following error
// message:
//
//   error: inherited virtual base class 'process::implementation::param' has
//   private destructor
// ---------------------------------------------------------------------------
{
protected:
  typedef firewall::deep_const_ptr<::std::ostream*> ostream_ptr;
  //
  typedef ::boost::iostreams::file_descriptor_sink   sink_type;
  typedef ::boost::iostreams::file_descriptor_source source_type;
  //
  typedef ::boost::iostreams::stream<sink_type> ostream_type;
  //
  typedef ::std::string string_type;
  //
public:
  ~bind_ostream();
  //
protected:
  explicit bind_ostream();
  //
  explicit bind_ostream(string_type const& pathname);
  //
  explicit bind_ostream(::std::ostream& os);
  //
public:
  string_type const&
  pathname() const {
    return _p;
  }
  //
protected:
  using param::initialize;
  //
  ::boost::optional<::boost::process::pipe>
  initialize(interface_type const& interface,
             context_type&         context,
             ostream_type&         os);
  //
  void
  finalize(interface_type const& interface, context_type& context);
  //
protected:
  using param::initializer;
  //
  template <class T>
  T const&
  initializer(::boost::optional<T>& i) const {
    if (!i) {
      if (!_s.is_open())
        PROCESS_THROW(closed_file_descriptor());
      i.emplace(_s);
    }
    return *i;
  }
  //
private:
  string_type const _p;
  //
private:
  ostream_ptr _os;
  //
private:
  source_type _s;
}; // class bind_ostream
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_BIND_OSTREAM_HPP_INCLUDED
