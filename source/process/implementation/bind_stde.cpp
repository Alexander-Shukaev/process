/// Preamble {{{
//  ==========================================================================
//        @file bind_stde.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:53:16 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-09 Tuesday 21:20:42 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "bind_stde.hpp"
//
# include "bind_stdo_to_stde.hpp"
# include "child.hpp"
# include "context.hpp"
// clang-format on
//
using std::istream;
//
namespace process {
namespace implementation {
bind_stde::~bind_stde() = default;
//
bind_stde::bind_stde(bool close_fds) : _close_fds(close_fds) {
}
//
bind_stde::bind_stde(string_type const& pathname, bool close_fds)
    : bind_istream(pathname), _close_fds(close_fds) {
}
//
bind_stde::bind_stde(::istream& is, bool close_fds)
    : bind_istream(is), _close_fds(close_fds) {
}
//
void*
bind_stde::new_alternate() const {
  return new bind_stdo_to_stde(*this);
}
//
bool
bind_stde::close_fds() const {
  return _close_fds;
}
//
void
bind_stde::initialize(interface_type const& interface,
                      context_type&         context) {
  if ((context.bind_stde.pipe =
           bind_istream::initialize(interface, context, context.c._stde))) {
    if (_close_fds) {
      context.close(context.bind_stde.pipe->source);
      context.close(context.bind_stde.pipe->sink);
    }
  }
}
//
void
bind_stde::finalize(interface_type const& interface, context_type& context) {
  bind_istream::finalize(interface, context);
}
//
platform::initializer const&
bind_stde::initializer() const {
  return bind_istream::initializer(_i);
}
} // namespace implementation
} // namespace process
