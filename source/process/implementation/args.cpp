/// Preamble {{{
//  ==========================================================================
//        @file args.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-10 Tuesday 01:10:05 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-10 Sunday 11:23:17 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "args.hpp"
//
# include "child.hpp"
# include "context.hpp"
//
# include "../param.hpp"
//
# include <iterator>
// clang-format on
//
using boost::none;
//
using std::next;
using std::prev;
//
namespace process {
namespace implementation {
args::~args() = default;
//
args::args(initializer_list_type const& initializer_list)
    : _c(initializer_list) {
}
//
args::args(container_type const& container) : _c(container) {
}
//
args&
args::splice(container_type::size_type index, container_type& container) {
  _c.splice((index > _c.size() / 2)
                ? ::prev(_c.cend(), index < _c.size() ? _c.size() - index : 0)
                : ::next(_c.cbegin(), index),
            container);
  _i = ::none;
  return *this;
}
//
args&
args::splice(container_type::size_type index, container_type&& container) {
  _c.splice((index > _c.size() / 2)
                ? ::prev(_c.cend(), index < _c.size() ? _c.size() - index : 0)
                : ::next(_c.cbegin(), index),
            container);
  _i = ::none;
  return *this;
}
//
args::container_type const&
args::container() const {
  return _c;
}
//
void
args::initialize(interface_type const& interface, context_type& context) {
  reinterpret_cast<interface_type&>(context.c._a).*PROCESS_PARAM_I_PTR =
      interface.*PROCESS_PARAM_I_PTR;
}
//
void
args::adjust(interface_type const& interface, context_type& context) {
  // TODO: This should be moved to another place when point #1 is implemented.
  if (_i ||
      reinterpret_cast<interface_type&>(context.c._a).*PROCESS_PARAM_I_PTR !=
          interface.*PROCESS_PARAM_I_PTR)
    return;
  if (_c.empty()) {
    // -----------------------------------------------------------------------
    { // clang-format off
# if 0
    _i.emplace(initializer_list_type{""});
# endif
    } // clang-format on
    //
    // NOTE:
    //
    // Results in the "Invalid syscall param" and "Uninitialized Memory Read"
    // diagnostics.
  } else
    _i.emplace(_c);
}
//
platform::initializer const&
args::initializer() const {
  // TODO: This should unconditionally return `param::initializer()' when
  // point #1 is implemented.
  if (_i)
    return *_i;
  return param::initializer();
}
} // namespace implementation
} // namespace process
