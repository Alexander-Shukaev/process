/// Preamble {{{
//  ==========================================================================
//        @file bind_stdo_to_stde.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:53:46 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-04-28 Thursday 22:05:21 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_BIND_STDO_TO_STDE_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_BIND_STDO_TO_STDE_HPP_INCLUDED
//
# include "bind_stdo.hpp"
# include "bind_stde.hpp"
//
# include "platform.hpp"
# include PROCESS_IMPLEMENTATION_PLATFORM_MODULE(pair_initializer)
          PROCESS_IMPLEMENTATION_PLATFORM_IMPORT(pair_initializer)
// clang-format on
//
namespace process {
namespace implementation {
class bind_stdo_to_stde final : private bind_stdo, private bind_stde {
  typedef platform::pair_initializer initializer_type;
  //
public:
  explicit bind_stdo_to_stde(bool close_fds);
  //
  explicit bind_stdo_to_stde(bind_stde const& e);
  //
private:
  virtual void*
  new_alternate() const override;
  //
private:
  virtual void
  initialize(interface_type const& interface, context_type& context) override;
  //
  virtual void
  finalize(interface_type const& interface, context_type& context) override;
  //
private:
  virtual platform::initializer const&
  initializer() const override;
  //
private:
  ::boost::optional<initializer_type> mutable _i;
}; // class bind_stdo_to_stde
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_BIND_STDO_TO_STDE_HPP_INCLUDED
