/// Preamble {{{
//  ==========================================================================
//        @file exe.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-06 Tuesday 23:48:06 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-03 Tuesday 22:54:02 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_EXE_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_EXE_HPP_INCLUDED
//
# include "param.hpp"
//
# include "platform.hpp"
# include PROCESS_IMPLEMENTATION_PLATFORM_MODULE(exe)
          PROCESS_IMPLEMENTATION_PLATFORM_IMPORT(exe)
//
# include <boost/optional.hpp>
// clang-format on
//
namespace process {
namespace implementation {
// ---------------------------------------------------------------------------
class exe : public virtual param
//
// CAUTION:
//
// LLVM/Clang complains about `private' inheritance with the following error
// message:
//
//   error: inherited virtual base class 'process::implementation::param' has
//   private destructor
// ---------------------------------------------------------------------------
{
  typedef platform::exe initializer_type;
  //
public:
  typedef ::std::string pathname_type;
  //
public:
  ~exe();
  //
public:
  explicit exe(pathname_type const& pathname);
  //
public:
  pathname_type const&
  pathname() const;
  //
protected:
  virtual void
  initialize(interface_type const& interface, context_type& context) override;
  //
  virtual void
  adjust(interface_type const& interface, context_type& context) override;
  //
protected:
  virtual platform::initializer const&
  initializer() const override;
  //
private:
  pathname_type _p;
  //
private:
  ::boost::optional<initializer_type> mutable _i;
}; // class exe
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_EXE_HPP_INCLUDED
