/// Preamble {{{
//  ==========================================================================
//        @file bind_stdi.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:47:19 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-01 Sunday 01:34:27 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "bind_stdi.hpp"
//
# include "child.hpp"
# include "context.hpp"
// clang-format on
//
using std::ostream;
//
namespace process {
namespace implementation {
bind_stdi::bind_stdi(bool close_fds) : _close_fds(close_fds) {
}
//
bind_stdi::bind_stdi(string_type const& pathname, bool close_fds)
    : bind_ostream(pathname), _close_fds(close_fds) {
}
//
bind_stdi::bind_stdi(::ostream& os, bool close_fds)
    : bind_ostream(os), _close_fds(close_fds) {
}
//
bool
bind_stdi::close_fds() const {
  return _close_fds;
}
//
void
bind_stdi::initialize(interface_type const& interface,
                      context_type&         context) {
  if ((context.bind_stdi.pipe =
           bind_ostream::initialize(interface, context, context.c._stdi))) {
    if (_close_fds) {
      context.close(context.bind_stdi.pipe->source);
      context.close(context.bind_stdi.pipe->sink);
    }
  }
}
//
void
bind_stdi::finalize(interface_type const& interface, context_type& context) {
  bind_ostream::finalize(interface, context);
}
//
platform::initializer const&
bind_stdi::initializer() const {
  return bind_ostream::initializer(_i);
}
} // namespace implementation
} // namespace process
