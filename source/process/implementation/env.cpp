/// Preamble {{{
//  ==========================================================================
//        @file env.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-05-22 Sunday 03:51:18 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-05 Thursday 23:18:39 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "env.hpp"
// clang-format on
//
using std::initializer_list;
using std::string;
using std::vector;
//
namespace process {
namespace implementation {
env::env(::initializer_list<::string> const& strings)
    : _strings(strings), _i(_strings) {
}
//
env::env(::vector<::string> const& strings)
    : _strings(strings), _i(_strings) {
}
//
platform::initializer const&
env::initializer() const {
  return _i;
}
} // namespace implementation
} // namespace process
