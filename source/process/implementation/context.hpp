/// Preamble {{{
//  ==========================================================================
//        @file context.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-05-22 Sunday 16:29:12 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-04-24 Sunday 23:38:58 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_CONTEXT_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_CONTEXT_HPP_INCLUDED
//
# include "fd.hpp"
//
# include <boost/process/pipe.hpp>
//
# include <boost/optional.hpp>
//
# include <unordered_set>
// clang-format on
//
namespace process {
namespace implementation {
class child;
//
struct context {
  context(child& c);
  //
  context(child* c);
  //
  bool
  close(platform::fd fd);
  //
  child& c;
  //
  struct bind_stde {
    ::boost::optional<::boost::process::pipe> pipe;
  } bind_stde;
  //
  struct bind_stdi {
    ::boost::optional<::boost::process::pipe> pipe;
  } bind_stdi;
  //
  struct bind_stdo {
    ::boost::optional<::boost::process::pipe> pipe;
  } bind_stdo;
  //
  struct close_fds {
    ::std::unordered_set<platform::fd> fds;
  } close_fds;
}; // struct context
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_CONTEXT_HPP_INCLUDED
