/// Preamble {{{
//  ==========================================================================
//        @file bind_istream.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:48:36 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 23:35:57 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_BIND_ISTREAM_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_BIND_ISTREAM_HPP_INCLUDED
//
# include "param.hpp"
//
# include "../closed_file_descriptor.hpp"
# include "../throw.hpp"
//
# include <process/firewall/deep_const_ptr>
//
# include <boost/process/pipe.hpp>
//
# include <boost/iostreams/device/file_descriptor.hpp>
# include <boost/iostreams/stream.hpp>
//
# include <boost/optional.hpp>
// clang-format on
//
namespace process {
namespace implementation {
// ---------------------------------------------------------------------------
class bind_istream : public virtual param
//
// BUG:
//
// Removing `virtual' from inheritance above erroneously causes
// `param::initialize(context&)' to dispatch to `bind_stdo::initializer()'
// which is a complete nonsense.  Is it an undefined behavior resulting from
// violation of the C++ Standard or a GCC bug?
//
// CAUTION:
//
// LLVM/Clang complains about `private' inheritance with the following error
// message:
//
//   error: inherited virtual base class 'process::implementation::param' has
//   private destructor
// ---------------------------------------------------------------------------
{
protected:
  typedef firewall::deep_const_ptr<::std::istream*> istream_ptr;
  //
  typedef ::boost::iostreams::file_descriptor_source source_type;
  typedef ::boost::iostreams::file_descriptor_sink   sink_type;
  //
  typedef ::boost::iostreams::stream<source_type> istream_type;
  //
  typedef ::std::string string_type;
  //
public:
  ~bind_istream();
  //
protected:
  explicit bind_istream();
  //
  explicit bind_istream(string_type const& pathname);
  //
  explicit bind_istream(::std::istream& is);
  //
public:
  string_type const&
  pathname() const {
    return _p;
  }
  //
protected:
  using param::initialize;
  //
  ::boost::optional<::boost::process::pipe>
  initialize(interface_type const& interface,
             context_type&         context,
             istream_type&         is);
  //
  void
  finalize(interface_type const& interface, context_type& context);
  //
protected:
  using param::initializer;
  //
  template <class T>
  T const&
  initializer(::boost::optional<T>& i) const {
    if (!i) {
      if (!_s.is_open())
        PROCESS_THROW(closed_file_descriptor());
      i.emplace(_s);
    }
    return *i;
  }
  //
private:
  string_type const _p;
  //
private:
  istream_ptr _is;
  //
private:
  sink_type _s;
}; // class bind_istream
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_BIND_ISTREAM_HPP_INCLUDED
