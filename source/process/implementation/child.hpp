/// Preamble {{{
//  ==========================================================================
//        @file child.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-02-13 Monday 02:32:16 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 23:35:57 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_CHILD_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_CHILD_HPP_INCLUDED
//
# include "basic_child.hpp"
//
# include "../throw.hpp"
//
# include <boost/process/child.hpp>
//
# include <boost/iostreams/device/file_descriptor.hpp>
# include <boost/iostreams/stream.hpp>
//
# include <atomic>
# include <vector>
// clang-format on
//
namespace process {
class args;
class child;
class exe;
class param;
//
namespace implementation {
class child : public basic_child {
  friend class ::process::child;
  //
  friend class bind_stde;
  friend class bind_stdi;
  friend class bind_stdo;
  //
  friend class close_stde;
  friend class close_stdi;
  friend class close_stdo;
  //
  friend class detached;
  friend class terminate_on_close;
  //
  typedef ::boost::process::child child_type;
  //
  typedef ::boost::iostreams::file_descriptor_source source_type;
  typedef ::boost::iostreams::file_descriptor_sink   sink_type;
  //
  typedef ::boost::iostreams::stream<source_type> istream_type;
  typedef ::boost::iostreams::stream<sink_type>   ostream_type;
  //
public:
  typedef ::std::vector<param const*> params;
  //
public:
  struct deleter {
    void
    operator()(void* p) const;
  }; // struct deleter
  //
public:
  virtual ~child();
  //
public:
  explicit child(args const& a, params const& params);
  //
  explicit child(exe const& e, params const& params);
  //
public:
  int
  exit_status() const;
  //
public:
  bool
  alive() const;
  //
  bool
  detached() const noexcept;
  //
  bool
  joinable() const noexcept;
  //
  bool
  valid() const noexcept;
  //
  bool
  terminated() const noexcept;
  //
  int
  wait() const;
  //
public:
  void
  close(bool terminate_on_close);
  //
  void
  detach();
  //
  int
  join();
  //
  void
  terminate();
  //
private:
  void
  execute(param const& param1, param const& param2, params const& params);
  //
private:
  child_type _c;
  //
private:
  istream_type _stde;
  ostream_type _stdi;
  istream_type _stdo;
  //
private:
  // clang-format off
  ::std::atomic<bool>         _terminate_on_close;
  ::std::atomic<bool> mutable _terminated;
  ::std::atomic<bool> mutable _waitable;
  // clang-format on
  //
private:
  ::std::atomic<int> mutable _status;
}; // class child
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_CHILD_HPP_INCLUDED
