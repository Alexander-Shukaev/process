/// Preamble {{{
//  ==========================================================================
//        @file terminated.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-10 Tuesday 01:35:21 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-09-18 Sunday 23:48:11 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_POSIX_TERMINATED_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_POSIX_TERMINATED_HPP_INCLUDED
//
# include "../../throw.hpp"
//
# include <sys/wait.h>
// clang-format on
//
namespace process {
namespace implementation {
namespace posix {
template <class ChildT>
inline bool
terminated(ChildT const& c, int& s) {
  if (c.pid <= 0)
    return true;
  int   status = 0;
  pid_t pid    = ::waitpid(c.pid, &status, WNOHANG);
  if (pid == 0)
    return false;
  if (pid == c.pid) {
    if (WIFEXITED(status)) {
      s = status;
      return true;
    }
    return !(WIFSTOPPED(status) || WIFCONTINUED(status));
  }
  if (pid == -1) {
    if (PROCESS_LAST_SYSTEM_ERROR == ECHILD)
      return true;
    PROCESS_THROW_LAST_SYSTEM_ERROR("waitpid(2) failed");
  }
  throw;
}
} // namespace posix
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_POSIX_TERMINATED_HPP_INCLUDED
