/// Preamble {{{
//  ==========================================================================
//        @file pair_initializer.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-21 Sunday 21:52:44 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-11 Thursday 22:03:04 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "pair_initializer.hpp"
// clang-format on
//
using boost::process::executor;
//
namespace process {
namespace implementation {
namespace posix {
pair_initializer::pair_initializer(initializer const& i1,
                                   initializer const& i2)
    : _i1(i1), _i2(i2) {
}
//
void
pair_initializer::on_fork_setup(::executor& e) const {
  _i1.get().on_fork_setup(e);
  _i2.get().on_fork_setup(e);
}
//
void
pair_initializer::on_fork_error(::executor& e) const {
  _i1.get().on_fork_error(e);
  _i2.get().on_fork_error(e);
}
//
void
pair_initializer::on_fork_success(::executor& e) const {
  _i1.get().on_fork_success(e);
  _i2.get().on_fork_success(e);
}
//
void
pair_initializer::on_exec_setup(::executor& e) const {
  _i1.get().on_exec_setup(e);
  _i2.get().on_exec_setup(e);
}
//
void
pair_initializer::on_exec_error(::executor& e) const {
  _i1.get().on_exec_error(e);
  _i2.get().on_exec_error(e);
}
} // namespace posix
} // namespace implementation
} // namespace process
