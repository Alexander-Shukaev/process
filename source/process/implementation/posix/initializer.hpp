/// Preamble {{{
//  ==========================================================================
//        @file initializer.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-04-10 Sunday 20:22:43 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 23:35:57 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_POSIX_INITIALIZER_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_POSIX_INITIALIZER_HPP_INCLUDED
//
# include <boost/process/executor.hpp>
# include <boost/process/initializers.hpp>
//
# define PROCESS_IMPLEMENTATION_POSIX_INITIALIZER_DECLARATION(     TYPE, \
                                                              BASE_TYPE) \
    namespace process { \
    namespace implementation { \
    namespace posix { \
    class TYPE final : public initializer, \
                       public BASE_TYPE { \
      typedef BASE_TYPE base_type; \
    \
    public: \
      using base_type::base_type; \
    \
    private: \
      void on_fork_setup  (::boost::process::executor& e) const override; \
      void on_fork_error  (::boost::process::executor& e) const override; \
      void on_fork_success(::boost::process::executor& e) const override; \
      void on_exec_setup  (::boost::process::executor& e) const override; \
      void on_exec_error  (::boost::process::executor& e) const override; \
    }; \
    } \
    } \
    }
//
# define PROCESS_IMPLEMENTATION_POSIX_INITIALIZER_DEFINITION(TYPE) \
    namespace process { \
    namespace implementation { \
    namespace posix { \
    void \
    TYPE::on_fork_setup(::boost::process::executor& e) const { \
      base_type::on_fork_setup(e); \
    } \
    \
    void \
    TYPE::on_fork_error(::boost::process::executor& e) const { \
      base_type::on_fork_error(e); \
    } \
    \
    void \
    TYPE::on_fork_success(::boost::process::executor& e) const { \
      base_type::on_fork_success(e); \
    } \
    \
    void \
    TYPE::on_exec_setup(::boost::process::executor& e) const { \
      base_type::on_exec_setup(e); \
    } \
    \
    void \
    TYPE::on_exec_error(::boost::process::executor& e) const { \
      base_type::on_exec_error(e); \
    } \
    } \
    } \
    }
// clang-format on
//
namespace process {
namespace implementation {
namespace posix {
class initializer
    : protected ::boost::process::initializers::initializer_base {
  typedef ::boost::process::initializers::initializer_base base_type;
  //
public:
  virtual ~initializer();
  //
public:
  using base_type::base_type;
  //
public:
  // clang-format off
  virtual void on_fork_setup  (::boost::process::executor& e) const;
  virtual void on_fork_error  (::boost::process::executor& e) const;
  virtual void on_fork_success(::boost::process::executor& e) const;
  virtual void on_exec_setup  (::boost::process::executor& e) const;
  virtual void on_exec_error  (::boost::process::executor& e) const;
  // clang-format on
}; // class initializer
} // namespace posix
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_POSIX_INITIALIZER_HPP_INCLUDED
