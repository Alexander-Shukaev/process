/// Preamble {{{
//  ==========================================================================
//        @file to_exit_status.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-02-11 Saturday 23:43:17 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-09-18 Sunday 22:23:39 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_POSIX_TO_EXIT_STATUS_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_POSIX_TO_EXIT_STATUS_HPP_INCLUDED
//
# include <boost/process/mitigate.hpp>
// clang-format on
//
namespace process {
namespace implementation {
namespace posix {
inline int
to_exit_status(int s) noexcept {
  return BOOST_PROCESS_EXITSTATUS(s);
}
} // namespace posix
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_POSIX_TO_EXIT_STATUS_HPP_INCLUDED
