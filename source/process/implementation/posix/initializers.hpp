/// Preamble {{{
//  ==========================================================================
//        @file initializers.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-05-23 Monday 00:11:05 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-22 Sunday 22:45:56 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_POSIX_INITIALIZERS_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_POSIX_INITIALIZERS_HPP_INCLUDED
//
# include "initializer.hpp"
//
# include <boost/ptr_container/ptr_vector.hpp>
// clang-format on
//
namespace process {
namespace implementation {
namespace posix {
struct clone_allocator {
  template <class T>
  static T*
  allocate_clone(T const&) {
  }
  //
  template <class T>
  static void
  deallocate_clone(T const*) {
  }
};
//
class initializers final
    : public initializer,
      private ::boost::ptr_vector<initializer, clone_allocator> {
  typedef ::boost::ptr_vector<initializer, clone_allocator> base_type;
  //
public:
  using base_type::base_type;
  //
public:
  using base_type::push_back;
  //
private:
  // clang-format off
  virtual void on_fork_setup  (::boost::process::executor& e) const override;
  virtual void on_fork_error  (::boost::process::executor& e) const override;
  virtual void on_fork_success(::boost::process::executor& e) const override;
  virtual void on_exec_setup  (::boost::process::executor& e) const override;
  virtual void on_exec_error  (::boost::process::executor& e) const override;
  // clang-format on
}; // class initializers
} // namespace posix
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_POSIX_INITIALIZERS_HPP_INCLUDED
