/// Preamble {{{
//  ==========================================================================
//        @file pair_initializer.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-21 Sunday 21:49:57 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 23:35:57 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_POSIX_PAIR_INITIALIZER_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_POSIX_PAIR_INITIALIZER_HPP_INCLUDED
//
# include "initializer.hpp"
//
# include <functional>
// clang-format on
//
namespace process {
namespace implementation {
namespace posix {
class pair_initializer final : public initializer {
public:
  pair_initializer(initializer const& i1, initializer const& i2);
  //
private:
  // clang-format off
  virtual void on_fork_setup  (::boost::process::executor& e) const override;
  virtual void on_fork_error  (::boost::process::executor& e) const override;
  virtual void on_fork_success(::boost::process::executor& e) const override;
  virtual void on_exec_setup  (::boost::process::executor& e) const override;
  virtual void on_exec_error  (::boost::process::executor& e) const override;
  // clang-format on
  //
private:
  ::std::reference_wrapper<initializer const> _i1;
  ::std::reference_wrapper<initializer const> _i2;
}; // class pair_initializer
} // namespace posix
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_POSIX_PAIR_INITIALIZER_HPP_INCLUDED
