/// Preamble {{{
//  ==========================================================================
//        @file close_ostream.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:54:51 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-06 Friday 21:28:58 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_CLOSE_OSTREAM_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_CLOSE_OSTREAM_HPP_INCLUDED
//
# include "param.hpp"
//
# include <boost/iostreams/device/file_descriptor.hpp>
# include <boost/iostreams/stream.hpp>
// clang-format on
//
namespace process {
namespace implementation {
// ---------------------------------------------------------------------------
class close_ostream : public virtual param
//
// CAUTION:
//
// LLVM/Clang complains about `private' inheritance with the following error
// message:
//
//   error: inherited virtual base class 'process::implementation::param' has
//   private destructor
// ---------------------------------------------------------------------------
{
protected:
  typedef ::boost::iostreams::file_descriptor_sink sink_type;
  //
  typedef ::boost::iostreams::stream<sink_type> ostream_type;
  //
protected:
  explicit close_ostream();
  //
protected:
  using param::initialize;
  //
  void
  initialize(interface_type const& interface,
             context_type&         context,
             ostream_type&         os);
}; // class close_ostream
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_CLOSE_OSTREAM_HPP_INCLUDED
