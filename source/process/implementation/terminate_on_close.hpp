/// Preamble {{{
//  ==========================================================================
//        @file terminate_on_close.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:56:13 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-03 Tuesday 21:52:22 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_TERMINATE_ON_CLOSE_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_TERMINATE_ON_CLOSE_HPP_INCLUDED
//
# include "param.hpp"
// clang-format on
//
namespace process {
namespace implementation {
class terminate_on_close final : private param {
public:
  explicit terminate_on_close(bool value);
  //
private:
  virtual void
  initialize(interface_type const& interface, context_type& context) override;
  //
private:
  virtual bool
  has_initializer() const override;
  //
  virtual platform::initializer const&
  initializer() const override;
  //
private:
  bool _v;
}; // class terminate_on_close
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_TERMINATE_ON_CLOSE_HPP_INCLUDED
