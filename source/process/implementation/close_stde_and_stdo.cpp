/// Preamble {{{
//  ==========================================================================
//        @file close_stde_and_stdo.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:55:50 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-06 Friday 20:15:53 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "close_stde_and_stdo.hpp"
//
# include "../throw.hpp"
# include "../unsupported_operation.hpp"
// clang-format on
//
namespace process {
namespace implementation {
close_stde_and_stdo::close_stde_and_stdo()
    : _i(close_stde::initializer(), close_stdo::initializer()) {
}
//
void*
close_stde_and_stdo::new_alternate() const {
  PROCESS_THROW(unsupported_operation());
}
//
void
close_stde_and_stdo::initialize(interface_type const& interface,
                                context_type&         context) {
  close_stde::initialize(interface, context);
  close_stdo::initialize(interface, context);
}
//
platform::initializer const&
close_stde_and_stdo::initializer() const {
  return _i;
}
} // namespace implementation
} // namespace process
