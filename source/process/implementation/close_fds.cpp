/// Preamble {{{
//  ==========================================================================
//        @file close_fds.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:43:16 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-22 Sunday 01:48:50 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "close_fds.hpp"
//
# include "context.hpp"
// clang-format on
//
using std::begin;
using std::end;
using std::initializer_list;
using std::vector;
//
namespace process {
namespace implementation {
close_fds::close_fds() = default;
//
close_fds::close_fds(::initializer_list<platform::fd> const& fds)
    : _fds(fds), _i(_fds) {
}
//
close_fds::close_fds(::vector<platform::fd> const& fds)
    : _fds(fds), _i(_fds) {
}
//
void
close_fds::initialize(interface_type const& interface,
                      context_type&         context) {
  (void)interface;
  if (!_i)
    _fds.insert(::end(_fds),
                ::begin(context.close_fds.fds),
                ::end(context.close_fds.fds));
}
//
void
close_fds::adjust(interface_type const& interface, context_type& context) {
  (void)interface;
  if (!_i)
    _fds.insert(::end(_fds),
                ::begin(context.close_fds.fds),
                ::end(context.close_fds.fds));
}
//
platform::initializer const&
close_fds::initializer() const {
  if (!_i)
    _i.emplace(_fds);
  return *_i;
}
} // namespace implementation
} // namespace process
