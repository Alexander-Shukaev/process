/// Preamble {{{
//  ==========================================================================
//        @file detached.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-02-13 Monday 02:05:39 (+0100)
//  --------------------------------------------------------------------------
//     @created 2017-02-13 Monday 02:05:35 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_DETACHED_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_DETACHED_HPP_INCLUDED
//
# include "param.hpp"
// clang-format on
//
namespace process {
namespace implementation {
class detached final : private param {
public:
  explicit detached(bool value);
  //
private:
  virtual void
  initialize(interface_type const& interface, context_type& context) override;
  //
private:
  virtual bool
  has_initializer() const override;
  //
  virtual platform::initializer const&
  initializer() const override;
  //
private:
  bool _v;
}; // class detached
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_DETACHED_HPP_INCLUDED
