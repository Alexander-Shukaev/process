/// Preamble {{{
//  ==========================================================================
//        @file get.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:11:09 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-19 Thursday 23:23:00 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_GET_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_GET_HPP_INCLUDED
//
# include "param.hpp"
//
# include "../param.hpp"
// clang-format on
//
namespace process {
namespace implementation {
inline param&
get(param::interface_type& interface) {
  return *(interface.*PROCESS_PARAM_I_PTR).get<param>();
}
//
inline param&
get(param::interface_type* interface) {
  return get(*interface);
}
//
inline param const&
get(param::interface_type const& interface) {
  return *(interface.*PROCESS_PARAM_I_PTR).get<param>();
}
//
inline param const&
get(param::interface_type const* interface) {
  return get(*interface);
}
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_GET_HPP_INCLUDED
