/// Preamble {{{
//  ==========================================================================
//        @file param.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:05:32 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 23:35:57 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_IMPLEMENTATION_PARAM_HPP_INCLUDED
# define PROCESS_IMPLEMENTATION_PARAM_HPP_INCLUDED
//
# include "initializer.fwd.hpp"
// clang-format on
//
namespace process {
class param;
//
namespace implementation {
class context;
//
class param {
public:
  typedef ::process::param interface_type;
  typedef context          context_type;
  //
public:
  struct deleter {
    void
    operator()(void* p) const;
  };
  //
public:
  virtual ~param();
  //
public:
  virtual void*
  new_alternate() const;
  //
public:
  virtual void
  initialize(interface_type const& interface, context_type& context);
  //
  virtual void
  adjust(interface_type const& interface, context_type& context);
  //
  virtual void
  finalize(interface_type const& interface, context_type& context);
  //
public:
  virtual bool
  has_initializer() const;
  //
  virtual platform::initializer const&
  initializer() const;
}; // class param
} // namespace implementation
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_IMPLEMENTATION_PARAM_HPP_INCLUDED
