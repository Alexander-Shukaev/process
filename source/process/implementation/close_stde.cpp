/// Preamble {{{
//  ==========================================================================
//        @file close_stde.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-05 Monday 23:50:32 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-09 Tuesday 21:20:42 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "close_stde.hpp"
//
# include "close_stde_and_stdo.hpp"
# include "child.hpp"
# include "context.hpp"
// clang-format on
//
namespace process {
namespace implementation {
close_stde::~close_stde() = default;
//
void*
close_stde::new_alternate() const {
  return new close_stde_and_stdo;
}
//
void
close_stde::initialize(interface_type const& interface,
                       context_type&         context) {
  close_istream::initialize(interface, context, context.c._stde);
}
//
platform::initializer const&
close_stde::initializer() const {
  return _i;
}
} // namespace implementation
} // namespace process
