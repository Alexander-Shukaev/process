/// Preamble {{{
//  ==========================================================================
//        @file param.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-10 Tuesday 22:05:48 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 22:20:45 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include <process/param>
//
# include <hacks/member_object_pointer>
//
# ifndef PROCESS_PARAM_I_PTR
// clang-format on
//
namespace process {
HACKS_MEMBER_OBJECT_POINTER_DECLARATION_BY_TAG(
    param_i_tag,
    PROCESS_FIREWALL_SHARED_IMPLEMENTATION_PTR_TYPE(void),
    param);
} // namespace process
//
namespace {}
//
// clang-format off
# define PROCESS_PARAM_I_PTR                                                 \
    HACKS_MEMBER_OBJECT_POINTER_BY_TAG(::process::param_i_tag)
# endif // PROCESS_PARAM_I_PTR
