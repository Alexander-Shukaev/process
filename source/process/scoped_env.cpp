/// Preamble {{{
//  ==========================================================================
//        @file scoped_env.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-10 Tuesday 01:31:11 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-08-26 Friday 23:18:56 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "scoped_env.hpp"
//
# include "rethrow.hpp"
//
# include <cstdlib>
# include <cstring>
// clang-format on
//
using std::strcpy;
using std::strlen;
//
namespace process {
namespace {
char const*
strdup(char const* p) {
  return p ? ::strcpy(new char[::strlen(p) + 1], p) : 0;
}
} // namespace
//
scoped_env::~scoped_env() {
  for (auto& v : _c) {
    if (v.second) {
      ::setenv(v.first.c_str(), v.second, 1);
      delete[] v.second;
    } else
      ::unsetenv(v.first.c_str());
  }
}
//
scoped_env::scoped_env(initializer_list_type const& initializer_list)
    : scoped_env(container_type(initializer_list)) {
}
//
scoped_env::scoped_env(container_type const& container) : _c(container) {
  try {
    for (auto& v : _c) {
      auto p = strdup(::getenv(v.first.c_str()));
      if (v.second) {
        if (::setenv(v.first.c_str(), v.second, 1))
          PROCESS_THROW_LAST_SYSTEM_ERROR("setenv(3) failed");
      } else {
        if (::unsetenv(v.first.c_str()))
          PROCESS_THROW_LAST_SYSTEM_ERROR("unsetenv(3) failed");
      }
      v.second = p;
    }
  }
  PROCESS_RETHROW_ALL
}
//
scoped_env::container_type const&
scoped_env::container() const {
  return _c;
}
} // namespace process
