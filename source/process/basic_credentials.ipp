/// Preamble {{{
//  ==========================================================================
//        @file basic_credentials.ipp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2018-09-28 Friday 02:02:18 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-09-26 Monday 23:26:54 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2018,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_BASIC_CREDENTIALS_IPP_INCLUDED
# define PROCESS_BASIC_CREDENTIALS_IPP_INCLUDED
//
# include "basic_credentials.hpp"
//
# include "args.hpp"
# include "elevation_filename.hpp"
# include "elevation_pathname.hpp"
# include "exe_pathname.hpp"
//
# include <boost/lexical_cast.hpp>
// clang-format on
//
using boost::lexical_cast;
//
using std::string;
//
namespace process {
namespace {
::string
elevation() {
  try {
    auto const pathname = exe_pathname(elevation_filename());
    if (!pathname.empty())
      return pathname;
  } catch (...) {
  }
  return elevation_pathname();
}
} // namespace
//
template <class P, class U>
basic_credentials<P, U>::~basic_credentials() = default;
//
template <class P, class U>
basic_credentials<P, U>::basic_credentials() : this_type(password_type()) {
}
//
template <class P, class U>
basic_credentials<P, U>::basic_credentials(password_type const& password)
    : this_type(password, "root") {
}
//
template <class P, class U>
basic_credentials<P, U>::basic_credentials(username_type const& username)
    : this_type("", username) {
}
//
template <class P, class U>
basic_credentials<P, U>::basic_credentials(uid_type const& uid)
    : this_type("", uid) {
}
//
template <class P, class U>
basic_credentials<P, U>::basic_credentials(password_type const& password,
                                           username_type const& username)
    : _password(password), _username(username) {
}
//
template <class P, class U>
basic_credentials<P, U>::basic_credentials(password_type const& password,
                                           uid_type const& uid)
    : this_type(
          password,
          '#' + ::lexical_cast<typename username_type::string_type>(uid)) {
}
//
template <class P, class U>
basic_credentials<P, U>::operator bool() const {
  return !empty();
}
//
template <class P, class U>
bool basic_credentials<P, U>::operator!() const {
  return empty();
}
//
template <class P, class U>
bool
basic_credentials<P, U>::empty() const {
  return password().empty() && username().empty();
}
//
template <class P, class U>
args
basic_credentials<P, U>::to_elevation_args() const {
  return {elevation(), "-u", username(), "-p", "\n", "-k", "-S"};
}
//
template <class P, class U>
args&
basic_credentials<P, U>::to_elevation_args(args& a) const {
  a.splice(0, {elevation(), "-u", username(), "-p", "\n", "-k", "-S"});
  return a;
}
//
template <class P, class U>
typename //
    basic_credentials<P, U>::password_type const&
    basic_credentials<P, U>::password() const {
  return _password;
}
//
template <class P, class U>
typename //
    basic_credentials<P, U>::username_type const&
    basic_credentials<P, U>::username() const {
  return _username;
}
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_BASIC_CREDENTIALS_IPP_INCLUDED
