/// Preamble {{{
//  ==========================================================================
//        @file stdo.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-08 Wednesday 19:45:25 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-08 Monday 19:44:02 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "stdo.hpp"
//
# include "invalid_argument.hpp"
# include "null_device.hpp"
# include "param.hpp"
# include "stde.hpp"
# include "throw.hpp"
# include "unsupported_operation.hpp"
# include "zero_device.hpp"
//
# include "implementation/bind_stdo.hpp"
# include "implementation/close_stdo.hpp"
# include "implementation/get.hpp"
// clang-format on
//
using std::istream;
using std::string;
//
namespace process {
stdo
stdo::close() {
  return stdo(new implementation::close_stdo);
}
//
stdo
stdo::none() {
  return stdo();
}
//
stdo
stdo::file(::string const& pathname, bool close_fds) {
  return stdo(new implementation::bind_stdo(pathname, close_fds));
}
//
stdo
stdo::file(char const* pathname, bool close_fds) {
  return stdo(
      new implementation::bind_stdo(pathname ? pathname : "", close_fds));
}
//
stdo
stdo::null() {
  return stdo(new implementation::bind_stdo(null_device(), true));
}
//
stdo
stdo::zero() {
  return stdo(new implementation::bind_stdo(zero_device(), true));
}
//
stdo
stdo::pipe(bool close_fds) {
  return stdo(new implementation::bind_stdo(close_fds));
}
//
stdo
stdo::pipe(::istream& is, bool close_fds) {
  return stdo(new implementation::bind_stdo(is, close_fds));
}
//
stdo
stdo::stde(class stde const& e) {
  try {
    return stdo(implementation::get(reinterpret_cast<param const&>(e))
                    .new_alternate());
  } catch (unsupported_operation const& e) {
    PROCESS_THROW(invalid_argument());
  }
}
//
stdo
stdo::stderr(class stde const& e) {
  return stde(e);
}
//
stdo::stdo() = default;
//
stdo::stdo(void* p) : param(p) {
}
} // namespace process
