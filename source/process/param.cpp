/// Preamble {{{
//  ==========================================================================
//        @file param.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-10 Tuesday 22:10:41 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 22:19:35 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "param.hpp"
//
# include "implementation/param.hpp"
// clang-format on
//
namespace process {
param::~param() = default;
//
param::param() : param(new implementation::param) {
}
//
param::param(void* p) : _i(p, implementation::param::deleter()) {
}
} // namespace process
//
HACKS_MEMBER_OBJECT_POINTER_DEFINITION_BY_TAG(::process::param_i_tag,
                                              ::process::param,
                                              _i);
