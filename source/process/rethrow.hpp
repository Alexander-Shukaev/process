/// Preamble {{{
//  ==========================================================================
//        @file rethrow.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-10 Tuesday 22:46:54 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-29 Friday 00:07:23 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_RETHROW_HPP_INCLUDED
# define PROCESS_RETHROW_HPP_INCLUDED
//
# include "closed_file_descriptor.hpp"
# include "closed_stream.hpp"
# include "env_error.hpp"
# include "invalid_argument.hpp"
# include "no_exit_status.hpp"
# include "unknown_error.hpp"
# include "unsupported_operation.hpp"
//
# include "system_error.hpp"
//
# include "logic_error.hpp"
# include "runtime_error.hpp"
//
# include "throw.hpp"
//
# define PROCESS_RETHROW                                                     \
      catch (::process::closed_file_descriptor const& e) {                   \
      throw;                                                                 \
    } catch (::process::closed_stream          const& e) {                   \
      throw;                                                                 \
    } catch (::process::env_error              const& e) {                   \
      throw;                                                                 \
    } catch (::process::invalid_argument       const& e) {                   \
      throw;                                                                 \
    } catch (::process::no_exit_status         const& e) {                   \
      throw;                                                                 \
    } catch (::process::unknown_error          const& e) {                   \
      throw;                                                                 \
    } catch (::process::unsupported_operation  const& e) {                   \
      throw;                                                                 \
    } catch (::process::system_error           const& e) {                   \
      throw;                                                                 \
    } catch (::process::logic_error            const& e) {                   \
      throw;                                                                 \
    } catch (::process::runtime_error          const& e) {                   \
      throw;                                                                 \
    } catch (::boost::system::system_error     const& e) {                   \
      PROCESS_THROW_WITH_NESTED(                                             \
          ::process::system_error(::std::make_error_code(                    \
              static_cast<::std::errc>(e.code().value()))));                 \
    } catch (::std::invalid_argument           const& e) {                   \
      PROCESS_THROW_WITH_NESTED(::process::invalid_argument(e));             \
    } catch (::std::system_error               const& e) {                   \
      PROCESS_THROW_WITH_NESTED(::process::system_error(e));                 \
    } catch (::std::logic_error                const& e) {                   \
      PROCESS_THROW_WITH_NESTED(::process::logic_error(e));                  \
    } catch (::std::runtime_error              const& e) {                   \
      PROCESS_THROW_WITH_NESTED(::process::runtime_error(e));                \
    } catch (::std::exception                  const& e) {                   \
      PROCESS_THROW_WITH_NESTED(::process::unknown_error(e));                \
    }
//
# define PROCESS_RETHROW_UNKNOWN                                             \
      catch (...) {                                                          \
      PROCESS_THROW(::process::unknown_error());                             \
    }
//
# define PROCESS_RETHROW_ALL                                                 \
    PROCESS_RETHROW                                                          \
    PROCESS_RETHROW_UNKNOWN
//
# define PROCESS_RETHROW_AND_CATCH(EXCEPTION_DECLARATION)                    \
    PROCESS_RETHROW catch (EXCEPTION_DECLARATION)
//
# endif // PROCESS_RETHROW_HPP_INCLUDED
