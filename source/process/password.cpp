/// Preamble {{{
//  ==========================================================================
//        @file password.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-10-15 Saturday 00:47:43 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-08 Monday 19:44:02 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "password.hpp"
// clang-format on
//
namespace process {
password::password() = default;
//
password::password(string_type const& s) : base_type(s) {
}
//
password::password(string_type&& s) : base_type(::std::move(s)) {
}
//
password::password(::process::env_var const& v) : password(v.get_or()) {
}
//
password::password(env_var const& v)
    : password(static_cast<::process::env_var const&>(v)) {
}
} // namespace process
