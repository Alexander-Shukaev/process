/// Preamble {{{
//  ==========================================================================
//        @file elevated_child.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2018-09-28 Friday 02:08:43 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 22:19:35 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2018,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "elevated_child.hpp"
//
# include "args.hpp"
# include "stdi.hpp"
//
# include <iostream>
// clang-format on
//
using std::endl;
using std::getline;
using std::string;
//
namespace process {
elevated_child::elevated_child() = default;
//
elevated_child::elevated_child(args_type const&   args,
                               params_type const& params)
    : elevated_child(credentials_type::env(), args, params) {
}
//
elevated_child::elevated_child(exe_type const& exe, params_type const& params)
    : elevated_child(credentials_type::env(), exe, params) {
}
//
elevated_child::elevated_child(credentials_type const& credentials,
                               args_type const&        args,
                               params_type const&      params)
    : child(
          [&credentials](args_type args) {
            // TODO: As per #2, don't splice here:
            return credentials.to_elevation_args(args);
          }(args),
          [](params_type params, class stdi const& i1, class stdi const& i2) {
            params.push_back(&reinterpret_cast<param const&>(i1));
            params.push_back(&reinterpret_cast<param const&>(i2));
            // TODO: Following #2, `push_back' extended
            // `process::args_terminator' and `process::exe_terminator' to
            // `params'.
            return params;
          }(params, stdi::close(), stdi::pipe())) {
  child::stdi() << credentials.password() << ::endl;
  if (child::is_stde_open()) {
    ::string s;
    ::getline(child::stde(), s);
  }
}
//
elevated_child::elevated_child(credentials_type const& credentials,
                               exe_type const&         exe,
                               params_type const&      params)
    : child(exe, params) {
  (void)credentials;
  // TODO:
  throw;
}
//
void
elevated_child::open(args_type const& args, params_type const& params) {
  open(credentials_type::env(), args, params);
}
//
void
elevated_child::open(exe_type const& exe, params_type const& params) {
  open(credentials_type::env(), exe, params);
}
//
void
elevated_child::open(credentials_type const& credentials,
                     args_type const&        args,
                     params_type const&      params) {
  child::open(
      [&credentials](args_type args) {
        // TODO: As per #2, don't splice here:
        return credentials.to_elevation_args(args);
      }(args),
      [](params_type params, class stdi const& i1, class stdi const& i2) {
        params.push_back(&reinterpret_cast<param const&>(i1));
        params.push_back(&reinterpret_cast<param const&>(i2));
        // TODO: Following #2, `push_back' extended `process::args_terminator'
        // and `process::exe_terminator' to `params'.
        return params;
      }(params, stdi::close(), stdi::pipe()));
  child::stdi() << credentials.password() << ::endl;
  if (child::is_stde_open()) {
    ::string s;
    ::getline(child::stde(), s);
  }
}
//
void
elevated_child::open(credentials_type const& credentials,
                     exe_type const&         exe,
                     params_type const&      params) {
  (void)credentials;
  child::open(exe, params);
  // TODO:
  throw;
}
} // namespace process
