/// Preamble {{{
//  ==========================================================================
//        @file env_var.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-10 Tuesday 01:30:37 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-09-28 Wednesday 22:50:14 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "env_var.hpp"
//
# include "rethrow.hpp"
//
# include <cstdlib>
// clang-format on
//
namespace process {
env_var::operator bool() const {
  return get();
}
//
bool env_var::operator!() const {
  return !get();
}
//
void
env_var::set(value_type value) {
  if (value) {
    try {
      if (::setenv(c_str(), value, 1))
        PROCESS_THROW_LAST_SYSTEM_ERROR("setenv(3) failed");
    }
    PROCESS_RETHROW_ALL
  } else
    unset();
}
//
void
env_var::set(::std::string const& value) {
  set(value.c_str());
}
//
void
env_var::reset() {
  unset();
}
//
void
env_var::unset() {
  try {
    if (::unsetenv(c_str()))
      PROCESS_THROW_LAST_SYSTEM_ERROR("unsetenv(3) failed");
  }
  PROCESS_RETHROW_ALL
}
//
env_var::value_type
env_var::get() const {
  return ::getenv(c_str());
}
//
env_var::value_type
env_var::get(value_type value) const {
  auto p = get();
  return p ? p : value;
}
//
env_var::value_type
env_var::get_or(value_type value) const {
  return get(value);
}
//
env_var::name_type const&
env_var::name() const {
  return *this;
}
//
env_var::value_type
env_var::value() const {
  return get();
}
//
env_var::value_type
env_var::value(value_type value) const {
  return get(value);
}
//
env_var::value_type
env_var::value_or(value_type value) const {
  return get(value);
}
} // namespace process
