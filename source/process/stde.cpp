/// Preamble {{{
//  ==========================================================================
//        @file stde.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-03-08 Wednesday 19:46:50 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-08 Monday 19:44:02 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "stde.hpp"
//
# include "invalid_argument.hpp"
# include "null_device.hpp"
# include "param.hpp"
# include "stdo.hpp"
# include "throw.hpp"
# include "unsupported_operation.hpp"
# include "zero_device.hpp"
//
# include "implementation/bind_stde.hpp"
# include "implementation/close_stde.hpp"
# include "implementation/get.hpp"
// clang-format on
//
using std::istream;
using std::string;
//
namespace process {
stde
stde::close() {
  return stde(new implementation::close_stde);
}
//
stde
stde::none() {
  return stde();
}
//
stde
stde::file(::string const& pathname, bool close_fds) {
  return stde(new implementation::bind_stde(pathname, close_fds));
}
//
stde
stde::file(char const* pathname, bool close_fds) {
  return stde(
      new implementation::bind_stde(pathname ? pathname : "", close_fds));
}
//
stde
stde::null() {
  return stde(new implementation::bind_stde(null_device(), true));
}
//
stde
stde::zero() {
  return stde(new implementation::bind_stde(zero_device(), true));
}
//
stde
stde::pipe(bool close_fds) {
  return stde(new implementation::bind_stde(close_fds));
}
//
stde
stde::pipe(::istream& is, bool close_fds) {
  return stde(new implementation::bind_stde(is, close_fds));
}
//
stde
stde::stdo(class stdo const& o) {
  try {
    return stde(implementation::get(reinterpret_cast<param const&>(o))
                    .new_alternate());
  } catch (unsupported_operation const& e) {
    PROCESS_THROW(invalid_argument());
  }
}
//
stde
stde::stdout(class stdo const& o) {
  return stdo(o);
}
//
stde::stde() = default;
//
stde::stde(void* p) : param(p) {
}
} // namespace process
