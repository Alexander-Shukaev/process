/// Preamble {{{
//  ==========================================================================
//        @file exe_pathname.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-03 Saturday 20:53:02 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-01 Sunday 15:48:11 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "exe_pathname.hpp"
//
# include "env_error.hpp"
# include "path_separator_char.hpp"
# include "rethrow.hpp"
//
# include <boost/process/search_path.hpp>
//
# include <boost/algorithm/string/join.hpp>
//
# include <boost/preprocessor/stringize.hpp>
//
# include <cstdlib>
// clang-format on
//
using boost::process::search_path;
//
using boost::algorithm::join;
//
using std::getenv;
using std::initializer_list;
using std::string;
using std::vector;
//
namespace process {
::string
exe_pathname(::string const& filename) {
  char const* p = ::getenv("PATH");
  if (p) {
    if (p[0]) {
      try {
        return ::search_path(filename, p);
      }
      PROCESS_RETHROW_ALL
    }
    PROCESS_THROW(env_error("Environment variable empty: PATH"));
  }
  PROCESS_THROW(env_error("Environment variable not set: PATH"));
}
//
::string
exe_pathname(::string const& filename, ::string const& path) {
  if (path.empty())
    return exe_pathname(filename);
  try {
    return ::search_path(filename, path);
  }
  PROCESS_RETHROW_ALL
}
//
::string
exe_pathname(::string const& filename, char const* path) {
  return exe_pathname(filename, ::string(path ? path : ""));
}
//
::string
exe_pathname(::string const&                     filename,
             ::initializer_list<::string> const& dirnames) {
  return exe_pathname(
      filename,
      ::join(dirnames, BOOST_PP_STRINGIZE(PROCESS_PATH_SEPARATOR_CHAR)));
}
//
::string
exe_pathname(::string const& filename, ::vector<::string> const& dirnames) {
  return exe_pathname(
      filename,
      ::join(dirnames, BOOST_PP_STRINGIZE(PROCESS_PATH_SEPARATOR_CHAR)));
}
} // namespace process
