/// Preamble {{{
//  ==========================================================================
//        @file credentials.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-10-15 Saturday 00:53:01 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-08-22 Monday 00:31:08 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "credentials.hpp"
//
# include "basic_credentials.ipp"
// clang-format on
//
namespace process {
template class basic_credentials<password, username>;
//
password::env_var const credentials::password_env_var(
    "PROCESS_CREDENTIALS_PASSWORD");
username::env_var const credentials::username_env_var(
    "PROCESS_CREDENTIALS_USERNAME");
} // namespace process
