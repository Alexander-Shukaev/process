/// Preamble {{{
//  ==========================================================================
//        @file args.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-06 Tuesday 21:05:36 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-08 Monday 19:44:02 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "args.hpp"
//
# include "implementation/args.hpp"
// clang-format on
//
namespace process {
args::args(initializer_list_type const& initializer_list)
    : param(new implementation::args(initializer_list)) {
}
//
args::args(container_type const& container)
    : param(new implementation::args(container)) {
}
//
args&
args::splice(container_type::size_type index, container_type& container) {
  _i.reset(new implementation::args(this->container()));
  _i.get<implementation::args>()->splice(index, container);
  return *this;
}
//
args&
args::splice(container_type::size_type index, container_type&& container) {
  _i.reset(new implementation::args(this->container()));
  _i.get<implementation::args>()->splice(index, container);
  return *this;
}
//
args::container_type const&
args::container() const {
  return _i.get<implementation::args>()->container();
}
} // namespace process
