/// Preamble {{{
//  ==========================================================================
//        @file password.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-10-15 Saturday 00:40:39 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-09-26 Monday 23:03:54 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_PASSWORD_HPP_INCLUDED
# define PROCESS_PASSWORD_HPP_INCLUDED
//
# include "basic_password"
//
# include "env_var"
//
# include <string>
// clang-format on
//
namespace process {
class PROCESS_GLOBAL password : public basic_password<::std::string> {
  typedef basic_password<string_type> base_type;
  //
public:
  class env_var : public ::process::env_var {
  public:
    using ::process::env_var::env_var;
    //
    env_var(::process::env_var const& v);
    //
    env_var(::process::env_var&& v);
  }; // class env_var
  //
public:
  using base_type::base_type;
  //
  password();
  //
  password(string_type const& s);
  //
  password(string_type&& s);
  //
  password(::process::env_var const& v);
  //
  password(env_var const& v);
}; // class password
} // namespace process
//
namespace {}
//
// clang-format off
# include "password.ipp"
//
# endif // PROCESS_PASSWORD_HPP_INCLUDED
