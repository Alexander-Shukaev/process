/// Preamble {{{
//  ==========================================================================
//        @file basic_credentials.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-21 Saturday 15:48:31 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-08-21 Sunday 15:09:38 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_BASIC_CREDENTIALS_HPP_INCLUDED
# define PROCESS_BASIC_CREDENTIALS_HPP_INCLUDED
//
# include "uid"
//
# include "visibility"
//
# include <iosfwd>
# include <type_traits>
// clang-format on
//
namespace process {
class args;
//
template <class P, class U>
class PROCESS_GLOBAL basic_credentials {
  static_assert(::std::is_class<P>::value, "");
  static_assert(::std::is_class<U>::value, "");
  //
  typedef basic_credentials<P, U> this_type;
  //
public:
  typedef P   password_type;
  typedef U   username_type;
  typedef uid uid_type;
  //
public:
  ~basic_credentials();
  //
public:
  explicit basic_credentials();
  //
  explicit basic_credentials(password_type const& password);
  //
  explicit basic_credentials(username_type const& username);
  //
  explicit basic_credentials(uid_type const& uid);
  //
  explicit basic_credentials(password_type const& password,
                             username_type const& username);
  //
  explicit basic_credentials(password_type const& password,
                             uid_type const&      uid);
  //
public:
  explicit operator bool() const;
  //
  // -------------------------------------------------------------------------
  bool operator!() const;
  //
  // NOTE:
  //
  // Clearly, `operator!' is redundant, but some compilers need it.
  // -------------------------------------------------------------------------
  //
public:
  bool
  empty() const;
  //
public:
  args
  to_elevation_args() const;
  //
  args&
  to_elevation_args(args& a) const;
  //
public:
  password_type const&
  password() const;
  //
  username_type const&
  username() const;
  //
private:
  password_type _password;
  username_type _username;
}; // class basic_credentials
//
template <class CharT, class TraitsT, class P, class U>
inline ::std::basic_ostream<CharT, TraitsT>&
operator<<(::std::basic_ostream<CharT, TraitsT>& os,
           basic_credentials<P, U> const&        bc) {
  (void)bc;
  return os;
}
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_BASIC_CREDENTIALS_HPP_INCLUDED
