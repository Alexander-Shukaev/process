/// Preamble {{{
//  ==========================================================================
//        @file username.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-10-15 Saturday 00:38:25 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-09-26 Monday 23:03:54 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_USERNAME_HPP_INCLUDED
# define PROCESS_USERNAME_HPP_INCLUDED
//
# include "basic_username"
//
# include "env_var"
//
# include <string>
// clang-format on
//
namespace process {
class PROCESS_GLOBAL username : public basic_username<::std::string> {
  typedef basic_username<string_type> base_type;
  //
public:
  class env_var : public ::process::env_var {
  public:
    using ::process::env_var::env_var;
    //
    env_var(::process::env_var const& v);
    //
    env_var(::process::env_var&& v);
  }; // class env_var
  //
public:
  using base_type::base_type;
  //
  username();
  //
  username(string_type const& s);
  //
  username(string_type&& s);
  //
  username(::process::env_var const& v);
  //
  username(env_var const& v);
}; // class username
} // namespace process
//
namespace {}
//
// clang-format off
# include "username.ipp"
//
# endif // PROCESS_USERNAME_HPP_INCLUDED
