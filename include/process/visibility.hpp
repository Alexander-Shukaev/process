/// Preamble {{{
//  ==========================================================================
//        @file visibility.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-06-04 Saturday 00:39:33 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-06-04 Saturday 00:38:51 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_VISIBILITY_HPP_INCLUDED
# define PROCESS_VISIBILITY_HPP_INCLUDED
//
# ifndef PROCESS_EXPORT
#   define PROCESS_EXPORT __attribute__((__visibility__("default")))
# endif
# ifndef PROCESS_IMPORT
#   define PROCESS_IMPORT __attribute__((__visibility__("default")))
# endif
# ifndef PROCESS_HIDDEN
#   define PROCESS_HIDDEN __attribute__((__visibility__("hidden")))
# endif
# ifdef PROCESS_STATIC
#   ifndef PROCESS_GLOBAL
#     define PROCESS_GLOBAL
#   endif
#   ifndef PROCESS_LOCAL
#     define PROCESS_LOCAL
#   endif
# else
#   ifndef PROCESS_GLOBAL
#     ifdef PROCESS_BUILD
#       define PROCESS_GLOBAL PROCESS_EXPORT
#     else
#       define PROCESS_GLOBAL PROCESS_IMPORT
#     endif
#   endif
#   ifndef PROCESS_LOCAL
#     define PROCESS_LOCAL PROCESS_HIDDEN
#   endif
# endif
//
# endif // PROCESS_VISIBILITY_HPP_INCLUDED
