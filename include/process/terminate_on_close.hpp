/// Preamble {{{
//  ==========================================================================
//        @file terminate_on_close.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-06-08 Wednesday 22:30:59 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-22 Sunday 22:00:06 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_TERMINATE_ON_CLOSE_HPP_INCLUDED
# define PROCESS_TERMINATE_ON_CLOSE_HPP_INCLUDED
//
# include "param"
// clang-format on
//
namespace process {
class PROCESS_GLOBAL terminate_on_close final : private param {
public:
  explicit terminate_on_close(bool value = true);
}; // class terminate_on_close
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_TERMINATE_ON_CLOSE_HPP_INCLUDED
