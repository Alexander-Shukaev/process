/// Preamble {{{
//  ==========================================================================
//        @file basic_username.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-29 Thursday 23:03:28 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-08-21 Sunday 15:09:38 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_BASIC_USERNAME_HPP_INCLUDED
# define PROCESS_BASIC_USERNAME_HPP_INCLUDED
//
# include "visibility"
//
# include <type_traits>
// clang-format on
//
namespace process {
template <class S>
class PROCESS_GLOBAL basic_username : public S {
  static_assert(::std::is_class<S>::value, "");
  //
public:
  typedef S string_type;
  //
public:
  using string_type::string_type;
  //
  basic_username();
  //
  basic_username(string_type const& s);
  //
  basic_username(string_type&& s);
}; // class basic_username
} // namespace process
//
namespace {}
//
// clang-format off
# include "basic_username.ipp"
//
# endif // PROCESS_BASIC_USERNAME_HPP_INCLUDED
