/// Preamble {{{
//  ==========================================================================
//        @file child.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-02-12 Sunday 02:46:07 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 21:47:36 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_CHILD_HPP_INCLUDED
# define PROCESS_CHILD_HPP_INCLUDED
//
# include "visibility"
//
# include <process/firewall/shared_implementation_ptr>
//
# include <iosfwd>
# include <vector>
// clang-format on
//
namespace process {
class args;
class exe;
class param;
//
class PROCESS_GLOBAL child {
public:
  // clang-format off
  typedef                             args   args_type;
  typedef                              exe    exe_type;
  typedef                            param  param_type;
  typedef ::std::vector<param_type const*> params_type;
  // clang-format on
  //
public:
  virtual ~child();
  //
public:
  explicit child();
  //
  explicit child(args_type const& args, params_type const& params);
  //
  explicit child(exe_type const& exe, params_type const& params);
  //
  template <class... Params>
  explicit child(args_type const& args, Params const&... params)
      : child(args, {&reinterpret_cast<param_type const&>(params)...}) {
  }
  //
  template <class... Params>
  explicit child(exe_type const& exe, Params const&... params)
      : child(exe, {&reinterpret_cast<param_type const&>(params)...}) {
  }
  //
public:
  explicit operator bool() const;
  //
  // -------------------------------------------------------------------------
  bool operator!() const;
  //
  // NOTE:
  //
  // Clearly, `operator!' is redundant, but some compilers need it.
  // -------------------------------------------------------------------------
  //
public:
  virtual void
  open(args_type const& args, params_type const& params);
  //
  virtual void
  open(exe_type const& exe, params_type const& params);
  //
  template <class... Params>
  void
  open(args_type const& args, Params const&... params) {
    open(args, {&reinterpret_cast<param_type const&>(params)...});
  }
  //
  template <class... Params>
  void
  open(exe_type const& exe, Params const&... params) {
    open(exe, {&reinterpret_cast<param_type const&>(params)...});
  }
  //
public:
  args_type const&
  get_args() const;
  //
  exe_type const&
  get_exe() const;
  //
public:
  int
  exit_status() const;
  //
public:
  bool
  detached() const;
  //
  bool
  joinable() const;
  //
  bool
  terminate_on_close() const;
  //
public:
  void
  close();
  //
  void
  close(bool terminate);
  //
  void
  detach();
  //
  int
  join();
  //
  void
  terminate();
  //
  int
  wait() const;
  //
public:
  bool
  is_stde_open() const;
  //
  bool
  is_stdi_open() const;
  //
  bool
  is_stdo_open() const;
  //
public:
  void
  close_stde();
  //
  void
  close_stdi();
  //
  void
  close_stdo();
  //
public:
  ::std::istream const&
  stde() const;
  //
  ::std::istream&
  stde();
  //
  ::std::ostream const&
  stdi() const;
  //
  ::std::ostream&
  stdi();
  //
  ::std::istream const&
  stdo() const;
  //
  ::std::istream&
  stdo();
  //
private:
  PROCESS_FIREWALL_SHARED_IMPLEMENTATION_PTR_MEMBER_DECLARATION(void, _i);
}; // class child
//
template <class CharT, class TraitsT>
inline ::std::basic_ostream<CharT, TraitsT>&
operator<<(::std::basic_ostream<CharT, TraitsT>& os, child const& c) {
  (void)c;
  return os;
}
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_CHILD_HPP_INCLUDED
