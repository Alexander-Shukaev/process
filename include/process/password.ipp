/// Preamble {{{
//  ==========================================================================
//        @file password.ipp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-10-15 Saturday 00:45:57 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-10-03 Monday 20:14:03 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_PASSWORD_IPP_INCLUDED
# define PROCESS_PASSWORD_IPP_INCLUDED
//
# include "password"
//
# include <utility>
// clang-format on
//
namespace process {
inline password::env_var::env_var(::process::env_var const& v)
    : ::process::env_var(v) {
}
//
inline password::env_var::env_var(::process::env_var&& v)
    : ::process::env_var(::std::move(v)) {
}
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_PASSWORD_IPP_INCLUDED
