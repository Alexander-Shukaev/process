/// Preamble {{{
//  ==========================================================================
//        @file env_var.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-21 Saturday 15:45:45 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-09-27 Tuesday 23:00:51 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_ENV_VAR_HPP_INCLUDED
# define PROCESS_ENV_VAR_HPP_INCLUDED
//
# include "visibility"
//
# include <iosfwd>
# include <string>
// clang-format on
//
namespace process {
class PROCESS_GLOBAL env_var : private ::std::string {
public:
  // clang-format off
  typedef ::std::string  name_type;
  typedef char const*   value_type;
  // clang-format on
  //
public:
  using name_type::name_type;
  //
public:
  explicit operator bool() const;
  //
  // -------------------------------------------------------------------------
  bool operator!() const;
  //
  // NOTE:
  //
  // Clearly, `operator!' is redundant, but some compilers need it.
  // -------------------------------------------------------------------------
  //
public:
  void
  set(value_type value);
  //
  void
  set(::std::string const& value);
  //
public:
  void
  reset();
  //
  void
  unset();
  //
public:
  value_type
  get() const;
  //
  value_type
  get(value_type value) const;
  //
  value_type
  get_or(value_type value = "") const;
  //
public:
  name_type const&
  name() const;
  //
  value_type
  value() const;
  //
  value_type
  value(value_type value) const;
  //
  value_type
  value_or(value_type value = "") const;
}; // class env_var
//
template <class CharT, class TraitsT>
inline ::std::basic_ostream<CharT, TraitsT>&
operator<<(::std::basic_ostream<CharT, TraitsT>& os, env_var const& ev) {
  (void)ev;
  return os;
}
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_ENV_VAR_HPP_INCLUDED
