/// Preamble {{{
//  ==========================================================================
//        @file exe_pathname.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-22 Monday 00:58:16 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-01 Sunday 15:20:02 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_EXE_PATHNAME_HPP_INCLUDED
# define PROCESS_EXE_PATHNAME_HPP_INCLUDED
//
# include "visibility"
//
# include <initializer_list>
# include <iterator>
# include <string>
# include <vector>
// clang-format on
//
namespace process {
PROCESS_GLOBAL
::std::string
exe_pathname(::std::string const& filename);
//
PROCESS_GLOBAL
::std::string
exe_pathname(::std::string const& filename, ::std::string const& path);
//
PROCESS_GLOBAL
::std::string
exe_pathname(::std::string const& filename, char const* path);
//
PROCESS_GLOBAL
::std::string
exe_pathname(::std::string const&                          filename,
             ::std::initializer_list<::std::string> const& dirnames);
//
PROCESS_GLOBAL
::std::string
exe_pathname(::std::string const&                filename,
             ::std::vector<::std::string> const& dirnames);
//
template <class Iterator>
::std::string
exe_pathname(::std::string const& filename, Iterator first, Iterator last) {
  return exe_pathname(filename, ::std::vector<::std::string>(first, last));
}
//
template <class Container>
::std::string
exe_pathname(::std::string const& filename, Container const& container) {
  return exe_pathname(
      filename, ::std::begin(container), ::std::end(container));
}
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_EXE_PATHNAME_HPP_INCLUDED
