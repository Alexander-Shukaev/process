/// Preamble {{{
//  ==========================================================================
//        @file stde.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-14 Sunday 20:32:00 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 14:33:10 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_STDE_HPP_INCLUDED
# define PROCESS_STDE_HPP_INCLUDED
//
# include "param"
//
# include <iosfwd>
# include <string>
// clang-format on
//
namespace process {
class stdo;
//
class PROCESS_GLOBAL stde final : private param {
public:
  static stde
  close();
  //
  static stde
  file(::std::string const& pathname, bool close_fds = true);
  //
  static stde
  file(char const* pathname, bool close_fds = true);
  //
  static stde
  none();
  //
  static stde
  null();
  //
  static stde
  zero();
  //
  static stde
  pipe(bool close_fds = true);
  //
  static stde
  pipe(::std::istream&, bool close_fds = true);
  //
  static stde
  stdo(class stdo const&);
  //
  static stde
  stdout(class stdo const&);
  //
public:
  explicit stde();
  //
private:
  explicit stde(void*);
}; // class stde
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_STDE_HPP_INCLUDED
