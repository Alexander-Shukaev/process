/// Preamble {{{
//  ==========================================================================
//        @file env.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-06-08 Wednesday 22:33:27 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-05 Thursday 22:57:13 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_ENV_HPP_INCLUDED
# define PROCESS_ENV_HPP_INCLUDED
//
# include "param"
//
# include <initializer_list>
# include <iterator>
# include <string>
# include <vector>
// clang-format on
//
namespace process {
class PROCESS_GLOBAL env final : private param {
public:
  static env
  inherit();
  //
public:
  explicit env();
  //
  env(::std::initializer_list<::std::string> const& strings);
  //
  explicit env(::std::vector<::std::string> const& strings);
  //
  template <class Iterator>
  explicit env(Iterator first, Iterator last)
      : env(::std::vector<::std::string>(first, last)) {
  }
  //
  template <class Container>
  explicit env(Container const& container)
      : env(::std::begin(container), ::std::end(container)) {
  }
}; // class env
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_ENV_HPP_INCLUDED
