/// Preamble {{{
//  ==========================================================================
//        @file args.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-21 Sunday 23:12:55 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 14:33:10 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_ARGS_HPP_INCLUDED
# define PROCESS_ARGS_HPP_INCLUDED
//
# include "param"
//
# include <initializer_list>
# include <iterator>
# include <list>
# include <string>
// clang-format on
//
namespace process {
class PROCESS_GLOBAL args final : private param {
public:
  // clang-format off
  typedef ::std::initializer_list<::std::string> initializer_list_type;
  typedef ::std::            list<::std::string>        container_type;
  // clang-format on
  //
public:
  args(initializer_list_type const& initializer_list);
  //
  explicit args(container_type const& container);
  //
  template <class Iterator>
  explicit args(Iterator first, Iterator last)
      : args(container_type(first, last)) {
  }
  //
  template <class Container>
  explicit args(Container const& container)
      : args(::std::begin(container), ::std::end(container)) {
  }
  //
public:
  args&
  splice(container_type::size_type index, container_type& container);
  //
  args&
  splice(container_type::size_type index, container_type&& container);
  //
public:
  container_type const&
  container() const;
}; // class args
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_ARGS_HPP_INCLUDED
