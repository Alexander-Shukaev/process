/// Preamble {{{
//  ==========================================================================
//        @file elevation_pathname.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-23 Tuesday 23:48:05 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-01 Sunday 18:24:10 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_ELEVATION_PATHNAME_HPP_INCLUDED
# define PROCESS_ELEVATION_PATHNAME_HPP_INCLUDED
//
# include "visibility"
//
# include <string>
// clang-format on
//
namespace process {
PROCESS_GLOBAL
::std::string const&
elevation_pathname();
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_ELEVATION_PATHNAME_HPP_INCLUDED
