/// Preamble {{{
//  ==========================================================================
//        @file stdi.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-14 Sunday 20:42:14 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-05-01 Sunday 01:23:59 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_STDI_HPP_INCLUDED
# define PROCESS_STDI_HPP_INCLUDED
//
# include "param"
//
# include <iosfwd>
# include <string>
// clang-format on
//
namespace process {
class PROCESS_GLOBAL stdi final : private param {
public:
  static stdi
  close();
  //
  static stdi
  file(::std::string const& pathname, bool close_fds = true);
  //
  static stdi
  file(char const* pathname, bool close_fds = true);
  //
  static stdi
  none();
  //
  static stdi
  null();
  //
  static stdi
  zero();
  //
  static stdi
  pipe(bool close_fds = true);
  //
  static stdi
  pipe(::std::ostream&, bool close_fds = true);
  //
public:
  explicit stdi();
  //
private:
  explicit stdi(void*);
}; // class stdi
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_STDI_HPP_INCLUDED
