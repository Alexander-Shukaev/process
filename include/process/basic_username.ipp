/// Preamble {{{
//  ==========================================================================
//        @file basic_username.ipp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-29 Thursday 23:25:59 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-08-21 Sunday 15:09:38 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_BASIC_USERNAME_IPP_INCLUDED
# define PROCESS_BASIC_USERNAME_IPP_INCLUDED
//
# include "basic_username"
//
# include <utility>
// clang-format on
//
namespace process {
template <class S>
inline basic_username<S>::basic_username() = default;
//
template <class S>
inline basic_username<S>::basic_username(string_type const& s)
    : string_type(s) {
}
//
template <class S>
inline basic_username<S>::basic_username(string_type&& s)
    : string_type(::std::move(s)) {
}
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_BASIC_USERNAME_IPP_INCLUDED
