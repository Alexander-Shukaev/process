/// Preamble {{{
//  ==========================================================================
//        @file elevated_child.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-27 Tuesday 01:02:28 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-08-21 Sunday 14:51:30 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_ELEVATED_CHILD_HPP_INCLUDED
# define PROCESS_ELEVATED_CHILD_HPP_INCLUDED
//
# include "child"
# include "credentials"
// clang-format on
//
namespace process {
class PROCESS_GLOBAL elevated_child : public child {
public:
  typedef credentials credentials_type;
  //
public:
  explicit elevated_child();
  //
  explicit elevated_child(args_type const& args, params_type const& params);
  //
  explicit elevated_child(exe_type const& exe, params_type const& params);
  //
  template <class... Params>
  explicit elevated_child(args_type const& args, Params const&... params)
      : elevated_child(args,
                       {&reinterpret_cast<param_type const&>(params)...}) {
  }
  //
  template <class... Params>
  explicit elevated_child(exe_type const& exe, Params const&... params)
      : elevated_child(exe,
                       {&reinterpret_cast<param_type const&>(params)...}) {
  }
  //
  explicit elevated_child(credentials_type const& credentials,
                          args_type const&        args,
                          params_type const&      params);
  //
  explicit elevated_child(credentials_type const& credentials,
                          exe_type const&         exe,
                          params_type const&      params);
  //
  template <class... Params>
  explicit elevated_child(credentials_type const& credentials,
                          args_type const&        args,
                          Params const&... params)
      : elevated_child(credentials,
                       args,
                       {&reinterpret_cast<param_type const&>(params)...}) {
  }
  //
  template <class... Params>
  explicit elevated_child(credentials_type const& credentials,
                          exe_type const&         exe,
                          Params const&... params)
      : elevated_child(credentials,
                       exe,
                       {&reinterpret_cast<param_type const&>(params)...}) {
  }
  //
public:
  virtual void
  open(args_type const& args, params_type const& params) override;
  //
  virtual void
  open(exe_type const& exe, params_type const& params) override;
  //
  template <class... Params>
  void
  open(args_type const& args, Params const&... params) {
    open(args, {&reinterpret_cast<param_type const&>(params)...});
  }
  //
  template <class... Params>
  void
  open(exe_type const& exe, Params const&... params) {
    open(exe, {&reinterpret_cast<param_type const&>(params)...});
  }
  //
  virtual void
  open(credentials_type const& credentials,
       args_type const&        args,
       params_type const&      params);
  //
  virtual void
  open(credentials_type const& credentials,
       exe_type const&         exe,
       params_type const&      params);
  //
  template <class... Params>
  void
  open(credentials_type const& credentials,
       args_type const&        args,
       Params const&... params) {
    open(
        credentials, args, {&reinterpret_cast<param_type const&>(params)...});
  }
  //
  template <class... Params>
  void
  open(credentials_type const& credentials,
       exe_type const&         exe,
       Params const&... params) {
    open(credentials, exe, {&reinterpret_cast<param_type const&>(params)...});
  }
}; // class elevated_child
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_ELEVATED_CHILD_HPP_INCLUDED
