/// Preamble {{{
//  ==========================================================================
//        @file closed_file_descriptor.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-06-08 Wednesday 22:27:49 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-04-28 Thursday 23:15:26 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_CLOSED_FILE_DESCRIPTOR_HPP_INCLUDED
# define PROCESS_CLOSED_FILE_DESCRIPTOR_HPP_INCLUDED
//
# include "logic_error"
// clang-format on
//
namespace process {
class PROCESS_GLOBAL closed_file_descriptor : public logic_error {
public:
  explicit closed_file_descriptor();
  //
public:
  using logic_error::logic_error;
}; // class closed_file_descriptor
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_CLOSED_FILE_DESCRIPTOR_HPP_INCLUDED
