/// Preamble {{{
//  ==========================================================================
//        @file credentials.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-10-15 Saturday 00:57:39 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-08-21 Sunday 15:09:38 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_CREDENTIALS_HPP_INCLUDED
# define PROCESS_CREDENTIALS_HPP_INCLUDED
//
# include "basic_credentials"
//
# include "password"
# include "username"
//
# include "visibility"
// clang-format on
//
namespace process {
class PROCESS_GLOBAL credentials
    : public basic_credentials<password, username> {
  typedef basic_credentials<password_type, username_type> base_type;
  //
public:
  static password_type::env_var const password_env_var;
  static username_type::env_var const username_env_var;
  //
public:
  static credentials
  env();
  //
public:
  using base_type::base_type;
};
} // namespace process
//
namespace {}
//
// clang-format off
# include "credentials.ipp"
//
# endif // PROCESS_CREDENTIALS_HPP_INCLUDED
