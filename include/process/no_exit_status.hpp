/// Preamble {{{
//  ==========================================================================
//        @file no_exit_status.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-09-19 Monday 00:28:43 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-04-28 Thursday 23:46:52 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_NO_EXIT_STATUS_HPP_INCLUDED
# define PROCESS_NO_EXIT_STATUS_HPP_INCLUDED
//
# include "runtime_error"
// clang-format on
//
namespace process {
class PROCESS_GLOBAL no_exit_status : public runtime_error {
public:
  explicit no_exit_status();
  //
public:
  explicit no_exit_status(::std::exception const& e);
  //
public:
  using runtime_error::runtime_error;
}; // class no_exit_status
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_NO_EXIT_STATUS_HPP_INCLUDED
