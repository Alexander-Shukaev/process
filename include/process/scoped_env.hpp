/// Preamble {{{
//  ==========================================================================
//        @file scoped_env.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-27 Saturday 04:38:01 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-07 Sunday 14:33:10 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef PROCESS_SCOPED_ENV_HPP_INCLUDED
# define PROCESS_SCOPED_ENV_HPP_INCLUDED
//
# include "visibility"
//
# include <unordered_map>
// clang-format on
//
namespace process {
class PROCESS_GLOBAL scoped_env {
  // clang-format off
  static void*
  operator new     (::std::size_t) = delete;
  //
  static void*
  operator new[]   (::std::size_t) = delete;
  //
  static void
  operator delete  (void*)         = delete;
  //
  static void
  operator delete[](void*)         = delete;
  // clang-format on
  //
public:
  // clang-format off
  typedef ::std::unordered_map   <::std::string, char const*>
      container_type;
  typedef ::std::initializer_list<container_type::value_type>
      initializer_list_type;
  // clang-format on
  //
public:
  ~scoped_env();
  //
public:
  scoped_env(initializer_list_type const& initializer_list);
  //
  explicit scoped_env(container_type const& container);
  //
  template <class Iterator>
  explicit scoped_env(Iterator first, Iterator last)
      : scoped_env(container_type(first, last)) {
  }
  //
  template <class Container>
  explicit scoped_env(Container const& container)
      : scoped_env(::std::begin(container), ::std::end(container)) {
  }
  //
public:
  container_type const&
  container() const;
  //
private:
  container_type _c;
}; // class scoped_env
} // namespace process
//
namespace {}
//
// clang-format off
# endif // PROCESS_SCOPED_ENV_HPP_INCLUDED
